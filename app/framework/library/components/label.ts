namespace Safe.Product.Library {
    export class SafeLabel extends Control {
        constructor() {
            super();

            this.positionProp.setIf(Position.Top);
            this.classNameProp.init('form-label');
            this.chosenStyle = this.styles[this.positionProp.toString()];

            this.seal(SafeLabel);
        }

        //region bindings
        static ngName = 'safeLabel';
        static transclude = true;

        public readonly positionProp: SmartEnum<Position>;
        public readonly forIdProp: SmartString;

        /* for html only
        private position: Position;
        private forId: string;
        */

        static exBindings: Binding[] = [
            new StaticBinding('position', Position, Position.Top).useSmartVar(),
            new StaticBinding('forId').useSmartVar()
        ];
        //endregion

        static template =
            `<label for="{{$ctrl.forId}}" ng-show="$ctrl.inShow" ng-style="$ctrl.chosenStyle">{{$ctrl.ioValue}}
                <ng-transclude>NOTHING TO LABEL!</ng-transclude>
            </label>
            `;

        private chosenStyle: string;
        private styles: StringMap = {
            Top: 'display: block',
            Left: 'display: inline-block; float: left; width: 15em; margin-right: 5px',
            Bottom: '', // TODO: Support bottom?
            Right: 'display: inline-block; float: right; width: 15em; margin-right: 5px'
        }
    }
}
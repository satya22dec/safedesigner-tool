namespace Safe.Product.Library {
    import Assert = Safe.System.Assert;
    import Debug = Safe.System.Debug;
    import ApplicationException = Safe.System.ApplicationException;
    export class Text extends InteractiveControl {
        static ngName = 'safeText';
        static $inject = ['$scope', '$element'];
        constructor(private $scope: IExtendedScope, private $element: JQuery) {
            super();
        }

        public $postLink() {
            super.$postLink();
        }

        static template =
`
<span id="{{$ctrl.id}}" 
    ng-class="$ctrl.classNameProp.forMarkup"
    ng-style="$ctrl.inStyle">{{$ctrl.ioValueProp.forMarkup}}</span>
`;
    }

    export class DataPager extends InteractiveControl {
        static ngName = 'safeDataPager';
        static $inject = ['$scope'];
        constructor(private $scope: ng.IScope) {
            super();

            this.classNameProp.setIf(['custom-angularstrap', 'pagination']);

            this.$scope.$on('pagination:startLoad', () => {
                this.fireEvent(SafeEventType.Load & SafeEventType.Before);
            });
            this.$scope.$on('pagination:loadPage', () => {
                this.fireEvent(SafeEventType.Load & SafeEventType.After);
            });
            this.$scope.$on('pagination:error', (event, status, config) => {
                throw new ApplicationException(`bgf-pagination had an error. Here's maybe some help: Event: ${JSON.stringify(event)}; Status: ${JSON.stringify(status)}; Config: ${JSON.stringify(config)}`);
            });

            this.seal(DataPager);
        }

        static exBindings: Bindings = [
            new InputBinding('inPerPage', VarType.Number, 10).useSmartVar(),
        ];

        //private inPerPage: number;
        public readonly inPerPageProp: SmartNumber;

        static template =
`<bgf-Pagination
    ng-class="$ctrl.classNameProp.forMarkup"
    collection="$ctrl.ioValue"
    url="$ctrl.inPut" 
    per-page="$ctrl.inPerPage">
</bgf-Pagination>
`;
    }
}
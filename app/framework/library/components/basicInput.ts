namespace Safe.Product.Library {

    export class LabeledControl extends FormControl {
        constructor() {
            super();
        }

        static require: StringMap = {
            label: '?^^safeLabel'
        };

        protected label: SafeLabel;

        public $postLink() {
            // set for attribute
            if (this.label) {
                this.label.forIdProp.set(this.idProp);
            }

            super.$postLink();
        }
    }

    export abstract class InputBase extends LabeledControl {
        constructor() {
            super();
        }

        protected handleKey(event: JQueryEventObject) {
            if (event.keyCode === KeyCodes.Return) {
                this.fireEvent(SafeEventType.Alert, this.ioValueProp);
            }
        }

        protected inputType: string;

        static template =
            `
            <input id="{{$ctrl.id}}" 
                name="{{$ctrl.name}}" 
                ng-disabled="$ctrl.inDisabled" 
                ng-show="$ctrl.inShow" 
                ng-class="$ctrl.classNameProp.forMarkup" 
                type="$ctrl.inputType" 
                ng-model="$ctrl.ioValue" 
                ng-keypress="$ctrl.handleKey($event)"/>
            `;
    }

    export class Input extends InputBase {
        constructor() {
            super();
            this.inputType = 'input';
        }

        static ngName = 'safeInput';
    }

    export class PlainButton extends InputBase {
        constructor() {
            super();
            this.inputType = 'button';
        }

        static ngName = 'safePlainButton';
    }

    export class Checkbox extends InputBase {
        constructor() {
            super();
            this.inputType = 'checkbox';
        }

        static exBindings: Bindings = [
            new InputBinding('inChecked')
        ];

        static ngName = 'safeCheckbox';
        static template =
            `
            <input id="{{$ctrl.id}}" 
                name="{{$ctrl.name}}" 
                ng-disabled="$ctrl.inDisabled" 
                ng-show="$ctrl.inShow" 
                ng-class="$ctrl.classNameProp.forMarkup" 
                type="$ctrl.inputType" 
                ng-model="$ctrl.ioValue" 
                ng-checked="$ctrl.ioValue"/>
            `;
    }

    export class RadioButtons extends FormControl {
        constructor() {
            super();

            this.nameProp.setIf('radioButtons');
        }

        static ngName = 'safeRadioButtons';
        static transclude = true;
        static template =
            `
            <div name="{{$ctrl.name}}" id="{{$ctrl.id}}">
                <ng-transclude></ng-transclude>
            </div>
            `;
    }

    export class RadioButton extends InputBase {
        constructor() {
            super();
            this.inputType = 'radio';
        }

        static require = { buttonGroup: '^^safeRadioButtons' }; // NOTE: <== *Requiring* - not optional
        static ngName = 'safeRadioButton';
        static template =
            `
            <input id="{{$ctrl.id}}" 
                name="{{$ctrl.name}}" 
                ng-disabled="$ctrl.inDisabled" 
                ng-show="$ctrl.inShow" 
                ng-class="$ctrl.classNameProp.forMarkup" 
                type="$ctrl.inputType" 
                ng-value="$ctrl.ioValue"
                ng-model="$ctrl.buttonGroup.ioValue"
                ng-keypress="$ctrl.handleKey($event)"/>
            `;

        public buttonGroup: RadioButtons;

        public $onInit() {
            super.$onInit();
            this.buttonGroup.addChild(this);
            this.nameProp.set(this.buttonGroup.nameProp + '-option');
        }

        public $onDestroy() {
            this.buttonGroup.removeChild(this);
        }
    }

    export class FancyButton extends LabeledControl {
        constructor() {
            super();
            throw new System.NotImplementedException();
        }

        // TODO: what to do here with or w/o transclusion? Should this only support a few internals, like am image or text?
        content: any;

        static ngName = 'safeButton';
        static template =
            `
            <button id="{{$ctrl.id}}" 
                name="{{$ctrl.name}}" 
                ng-disabled="$ctrl.inDisabled" 
                ng-show="$ctrl.inShow" 
                ng-class="$ctrl.classNameProp.forMarkup" 
                ng-model="$ctrl.ioValue" 
                ng-click="$ctrl.fireEvent('Exclaim', $ctrl.id)">
                <ng-transclude>{{$ctrl.content}}</ng-transclude>
            </button>
            `;
    }
}
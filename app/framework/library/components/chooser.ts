namespace Safe.Product.Library {
    export class Chooser extends InteractiveControl {
        constructor() {
            super();
            throw new System.NotImplementedException();
        }

        static ngName = 'safeChooser';
        static template =
            `
            <safe-Radio-Buttons id="{{$ctrl.id.value}}" out-Collection="$ctrl.buttons">
                
            </safe-Radio-Buttons>
            `;
    }
}
namespace Safe.Product.Library.Table {
    import Assert = System.Assert;
    import Debug = Safe.System.Debug;
    export class QuickTable extends ComplexControl {
        public static defaultClasses = ['table', 'responsive-table'];

        public static ngName = 'safeQuickTable';
        static $inject = ['$transclude', '$scope', '$element'];

        constructor(private $transclude: ng.ITranscludeFunction, private $scope: ng.IScope, private $element: JQuery) {
            super();

            this.classNameProp.set(QuickTable.defaultClasses);
            this.rowClassProp.set(['table-row']);
            this.displayed = this.inPutProp.value;

            let content = this.$transclude();
            this.createHeader(content);
            this.createFooter(content);

            this.seal(QuickTable);
        }

        public $onInit() {
            super.$onInit();
        }

        private createHeader(content: JQuery) {
            let insertionPoint = this.$element.find('table thead tr');
            let jColumns = content.filter(Column.ngName.dasherize());

            jColumns.each((idx, el) => {
                let jElement = $(el);
                let column: Column = jElement.controller(Column.ngName);
                Assert.hasValue(column);
                this.addChild(column);
                insertionPoint.append($('<th>').append(jElement));
            });
        }

        private createFooter(content: JQuery) {
            let jDataPager = content.filter(DataPager.ngName.dasherize());
            if (jDataPager.length) {
                let jFooter = $(QuickTable.footer);
                let insertionPoint = jFooter.find('tr td');
                insertionPoint.addClass('text-center table-row').append(jDataPager.children());

                this.$element.find('tbody').after(jFooter);
            }
        }

        public $postLink() {
            super.$postLink();
        }

        static exBindings: Binding[] = [
            new StaticBinding('rowClass', VarType.Bag).useSmartVar(),
            //new InputBinding('inPerPage', VarType.Number, 10).useSmartVar(),
            new InputBinding('inAuto', VarType.Boolean, false).useSmartVar(),
        ];

        //private rowClass: string[];
        public readonly rowClassProp: SmartStringBag;
        //private inPerPage: string[];
        //public readonly inPerPageProp: SmartNumber;
        //private inAuto: bool;
        public readonly inAutoProp: SmartBool;

        // internal "pool" of data, managed by smart table Third-Party component
        private displayed: {}[] = null;

        static transclude = true;
        static template =
`<table ng-class="$ctrl.classNameProp.forMarkup" 
    st-table="$ctrl.displayed" 
    st-safe-src="$ctrl.inPut">
    
    <thead>
        <tr>
        </tr>
    </thead>
    <tbody>
        <tr ng-class="$ctrl.rowClassProp.forMarkup" 
        ng-click="$ctrl.fireEvent('Alert')"
        ng-repeat="row in $ctrl.displayed"
        safe-Data-Row>
        </tr>
    </tbody>
 </table>
`;

        private static footer = `<tfoot><tr><td></td></tr></tfoot>`;
    }

    export class Column extends NamedControl {
        static defaultClasses = ['table-head'];

        static ngName = "safeTableColumn";
        static $inject = ['$transclude', '$scope', '$element'];

        constructor(public $transclude: ng.ITranscludeFunction, private $scope: ng.IScope, private $element: JQuery) {
            super();

            this.classNameProp.set(Column.defaultClasses);
            this.seal(Column);
        }

        static transclude = true;

        static template =
        // TODO: I HATE BINDING TO THE PARENT VALUES HERE. NOW THAT I FIGURED OUT TRANSCLUSION, FIX ASAP
`<span ng-class="$ctrl.classNameProp.forMarkup" 
    st-table="$ctrl.parent.displayed"
    st-safe-src="$ctrl.parent.inPut"
    st-sort="{{$ctrl.name}}">{{$ctrl.headerText}}</span>`;

        static exBindings: Binding[] = [
            new StaticBinding('headerText').useSmartVar()
        ];

        //private headerText: string;
        public readonly headerTextProp: SmartString;

        public $postLink() {
            super.$postLink();
        }
    }

    /** Internal use only - Directive used in table generation. Registered with Angular in module.ts */
    export class SafeDataRow implements ng.IDirective {
        static $inject = ['$compile'];

        constructor(private $compile: ng.ICompileService) {
            if (!(this instanceof SafeDataRow))
                return new SafeDataRow($compile);
        }

        public priority = 0;
        public restrict = 'A';

        // TODO: Since this bad boy executes all the friggin time for every row, multiple times,
        // TODO: look into caching the cell/markup JQuery objects for cloning. Dunno if this is a real problem yet.
        // TODO: Put a lot of this into factory methods
        public link = {
            pre: (scope: ng.IScope, instanceElement: JQuery) => {
                let table: QuickTable = scope.$ctrl;
                Assert.hasValue(table);
                let rowData = scope.row;

                _.each(table.getChildrenOfType(Column), (column: Column) => {
                    let name = column.nameProp.value;
                    let cellValue = rowData[name];

                    // new scope from my parent, non-isolated so bindings to $ctrl work right
                    // also add context to scope to allow transcluded components to access them.
                    let newScope = scope.$parent.$new();
                    newScope.rowData = rowData;
                    newScope.cellValue = rowData[name];

                    column.$transclude(newScope, (clone) => {
                        let newContent: JQuery = clone.filter('*').length ?
                            this.$compile(clone)(newScope) :
                            this.$compile($(`<span>{{cellValue}}</span>`))(newScope);

                        instanceElement.append($('<td>').addClass('cell-table').append(newContent));
                    });
                });
            }
        };
    }
}

/*
 // TODO: Any reason to walk transcluded components? Resurrect this code in some TBD form...

 //region --spelunk components
 // fixup new/transcluded components with data if needed
 // let components =
 //     newContent.filter((idx, el) => el.nodeType === Node.ELEMENT_NODE && el.tagName.toUpperCase().indexOf('SAFE-') === 0);
 //
 // components.each((idx, el) => {
 //     let jElement = $(el);
 //     let tagName = el.tagName;
 //     let ngName = tagName.dedasherize();
 //     let control: Control = jElement.controller(ngName);
 //     Assert.hasValue(control);
 //
 //     // control.inPutProp.setIf(rowData);
 //     // control.ioValueProp.setIf(cellValue);
 // });
 //endregion

 */
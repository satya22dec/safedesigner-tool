namespace Safe.Product.Library {
    export class DataFetcher extends ComplexControl {

        static $inject = ['$http'];
        constructor(protected $http: ng.IHttpService) {
            super();

        }

        public $postLink() {
            super.$postLink();

            this.$http.get(this.inPutProp.value).then((result) => {
                let data = result.data;
                this.ioValueProp.set(data);
            });
        }

        static ngName = 'safeFetch';
        static template = '<span style="display:none;visibility:hidden"></span>';
    }
}
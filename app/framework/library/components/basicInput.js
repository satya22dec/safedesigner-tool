var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Safe;
(function (Safe) {
    var Product;
    (function (Product) {
        var Library;
        (function (Library) {
            var LabeledControl = (function (_super) {
                __extends(LabeledControl, _super);
                function LabeledControl() {
                    return _super.call(this) || this;
                }
                LabeledControl.prototype.$postLink = function () {
                    // set for attribute
                    if (this.label) {
                        this.label.forIdProp.set(this.idProp);
                    }
                    _super.prototype.$postLink.call(this);
                };
                return LabeledControl;
            }(Safe.FormControl));
            LabeledControl.require = {
                label: '?^^safeLabel'
            };
            Library.LabeledControl = LabeledControl;
            var InputBase = (function (_super) {
                __extends(InputBase, _super);
                function InputBase() {
                    return _super.call(this) || this;
                }
                InputBase.prototype.handleKey = function (event) {
                    if (event.keyCode === Safe.KeyCodes.Return) {
                        this.fireEvent(Safe.SafeEventType.Alert, this.ioValueProp);
                    }
                };
                return InputBase;
            }(LabeledControl));
            InputBase.template = "\n            <input id=\"{{$ctrl.id}}\" \n                name=\"{{$ctrl.name}}\" \n                ng-disabled=\"$ctrl.inDisabled\" \n                ng-show=\"$ctrl.inShow\" \n                ng-class=\"$ctrl.classNameProp.forMarkup\" \n                type=\"$ctrl.inputType\" \n                ng-model=\"$ctrl.ioValue\" \n                ng-keypress=\"$ctrl.handleKey($event)\"/>\n            ";
            Library.InputBase = InputBase;
            var Input = (function (_super) {
                __extends(Input, _super);
                function Input() {
                    var _this = _super.call(this) || this;
                    _this.inputType = 'input';
                    return _this;
                }
                return Input;
            }(InputBase));
            Input.ngName = 'safeInput';
            Library.Input = Input;
            var PlainButton = (function (_super) {
                __extends(PlainButton, _super);
                function PlainButton() {
                    var _this = _super.call(this) || this;
                    _this.inputType = 'button';
                    return _this;
                }
                return PlainButton;
            }(InputBase));
            PlainButton.ngName = 'safePlainButton';
            Library.PlainButton = PlainButton;
            var Checkbox = (function (_super) {
                __extends(Checkbox, _super);
                function Checkbox() {
                    var _this = _super.call(this) || this;
                    _this.inputType = 'checkbox';
                    return _this;
                }
                return Checkbox;
            }(InputBase));
            Checkbox.exBindings = [
                new Safe.InputBinding('inChecked')
            ];
            Checkbox.ngName = 'safeCheckbox';
            Checkbox.template = "\n            <input id=\"{{$ctrl.id}}\" \n                name=\"{{$ctrl.name}}\" \n                ng-disabled=\"$ctrl.inDisabled\" \n                ng-show=\"$ctrl.inShow\" \n                ng-class=\"$ctrl.classNameProp.forMarkup\" \n                type=\"$ctrl.inputType\" \n                ng-model=\"$ctrl.ioValue\" \n                ng-checked=\"$ctrl.ioValue\"/>\n            ";
            Library.Checkbox = Checkbox;
            var RadioButtons = (function (_super) {
                __extends(RadioButtons, _super);
                function RadioButtons() {
                    var _this = _super.call(this) || this;
                    _this.nameProp.setIf('radioButtons');
                    return _this;
                }
                return RadioButtons;
            }(Safe.FormControl));
            RadioButtons.ngName = 'safeRadioButtons';
            RadioButtons.transclude = true;
            RadioButtons.template = "\n            <div name=\"{{$ctrl.name}}\" id=\"{{$ctrl.id}}\">\n                <ng-transclude></ng-transclude>\n            </div>\n            ";
            Library.RadioButtons = RadioButtons;
            var RadioButton = (function (_super) {
                __extends(RadioButton, _super);
                function RadioButton() {
                    var _this = _super.call(this) || this;
                    _this.inputType = 'radio';
                    return _this;
                }
                RadioButton.prototype.$onInit = function () {
                    _super.prototype.$onInit.call(this);
                    this.buttonGroup.addChild(this);
                    this.nameProp.set(this.buttonGroup.nameProp + '-option');
                };
                RadioButton.prototype.$onDestroy = function () {
                    this.buttonGroup.removeChild(this);
                };
                return RadioButton;
            }(InputBase));
            RadioButton.require = { buttonGroup: '^^safeRadioButtons' }; // NOTE: <== *Requiring* - not optional
            RadioButton.ngName = 'safeRadioButton';
            RadioButton.template = "\n            <input id=\"{{$ctrl.id}}\" \n                name=\"{{$ctrl.name}}\" \n                ng-disabled=\"$ctrl.inDisabled\" \n                ng-show=\"$ctrl.inShow\" \n                ng-class=\"$ctrl.classNameProp.forMarkup\" \n                type=\"$ctrl.inputType\" \n                ng-value=\"$ctrl.ioValue\"\n                ng-model=\"$ctrl.buttonGroup.ioValue\"\n                ng-keypress=\"$ctrl.handleKey($event)\"/>\n            ";
            Library.RadioButton = RadioButton;
            var FancyButton = (function (_super) {
                __extends(FancyButton, _super);
                function FancyButton() {
                    var _this = _super.call(this) || this;
                    throw new Safe.System.NotImplementedException();
                    return _this;
                }
                return FancyButton;
            }(LabeledControl));
            FancyButton.ngName = 'safeButton';
            FancyButton.template = "\n            <button id=\"{{$ctrl.id}}\" \n                name=\"{{$ctrl.name}}\" \n                ng-disabled=\"$ctrl.inDisabled\" \n                ng-show=\"$ctrl.inShow\" \n                ng-class=\"$ctrl.classNameProp.forMarkup\" \n                ng-model=\"$ctrl.ioValue\" \n                ng-click=\"$ctrl.fireEvent('Exclaim', $ctrl.id)\">\n                <ng-transclude>{{$ctrl.content}}</ng-transclude>\n            </button>\n            ";
            Library.FancyButton = FancyButton;
        })(Library = Product.Library || (Product.Library = {}));
    })(Product = Safe.Product || (Safe.Product = {}));
})(Safe || (Safe = {}));
//# sourceMappingURL=basicInput.js.map
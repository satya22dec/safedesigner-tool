// NOTE: Found this handy chunk of code on the internet...
// Fix Function#name on browsers that do not support it (IE):
if (!(function f() {
    }).name) {
    Object.defineProperty(Function.prototype, 'name', {
        get: function () {
            var name = this.toString().match(/^function\s*([^\s(]+)/)[1];
            // For better performance only parse once, and then cache the
            // result through a new accessor for repeated access.
            Object.defineProperty(this, 'name', {value: name});
            return name;
        }
    });
}

String.prototype.hashCode = function () {
    var hash = 0, i, chr;
    if (this.length === 0) return hash;
    for (i = 0; i < this.length; i++) {
        chr = this.charCodeAt(i);
        hash = ((hash << 5) - hash) + chr;
        hash |= 0; // Convert to 32bit integer
    }
    return hash;
};

String.prototype.dasherize = function() {
    return this.replace(/[A-Z]/g, function(char: string, index: number) {
        return (index !== 0 ? '-' : '') + char;
    });
};

String.prototype.dedasherize = function() {
    let words = this.split('-');

    for (let i=0; i<words.length; i++) {
        let firstChar = words[i].charAt(0);
        firstChar = i ? firstChar.toUpperCase() : firstChar.toLowerCase();
        let rest = words[i].substr(1).toLowerCase();
        words[i] = firstChar + rest;
    }
    return words.join('');
};

namespace Safe {
    export function randomId(base: string = 'random'): string {
        return `${base}:${_.random(Number.MAX_VALUE).toString(36).substr(0,10)}`;
    }

    export function isSmartVar<T>(obj: any): obj is SmartVar<T> {
        return _.isObject(obj) && (obj instanceof SmartVar);
    }

    export function smartVal<T>(input: LikeVar<T>): T {
        return isSmartVar(input) ? input.value : input;
    }
}
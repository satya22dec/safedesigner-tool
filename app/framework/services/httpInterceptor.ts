namespace Safe.System {
    /** This type's responsibility is simple: turn Http response errors into our Exception */
    export class HttpInterceptor implements ng.IHttpInterceptor {
        public static $inject = ['$q'];
        constructor(private $q: ng.IQService) {
            
        }

        public responseError = (rejection: IHttpRejection)=> {
            if ((rejection.status > HttpStatusCode.InternalServerError &&
                    rejection.status < HttpStatusCode.HttpVersionNotSupported) ||
                rejection.status === HttpStatusCode.E_FAIL) {

                throw new ServerUnavailableException(rejection);
            }

            if (rejection.status === HttpStatusCode.Unauthorized)
                return this.$q.reject(rejection);

            if (rejection.status === HttpStatusCode.UnProcessableEntity) {
                throw new ServerValidationException(rejection as IHttpServerValidationRejection);
            }


            throw new HttpException(rejection);

        }
    }
}
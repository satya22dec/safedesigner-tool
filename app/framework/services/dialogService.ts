namespace Safe.System {
    import bts = angular.ui.bootstrap;

    export class DialogService {
        static $inject = ['$uibModal', 'safeModule'];
        constructor(private modalService: bts.IModalService, private safeModule: Safe.System.Module) {
        }

        public popup(dialogComponent: IComponentClass|string, bindingData: any = {}, settings: bts.IModalSettings = {}): bts.IModalServiceInstance {
            throw new System.NotImplementedException();
            // // Lookup the "real" component class for the one they passed, so variations work
            // let name = _.isString(dialogComponent) ? dialogComponent.replace('-', '') : dialogComponent.ngName;
            // dialogComponent = this.safeModule.componentMaps.product[name];
            //
            // let dialogSettings: bts.IModalSettings = {
            //     // user-defined settings
            //     windowTopClass: settings.windowTopClass,
            //     backdropClass: settings.backdropClass,
            //     openedClass: settings.openedClass,
            //     windowClass: settings.windowClass,
            // };
            // let options: ng.IComponentOptions = {
            //     controller: dialogComponent,
            //     controllerAs: '$ctrl',
            //     template: dialogComponent.template,
            //     transclude: dialogComponent.transclude,
            //     // TODO: bindings: xxx
            // };
            //
            // // TODO: Support two-way binding (on return?)
            // if (_.any(_.values(options.bindings), v => v === '&' || v === '=')) {
            //     throw new ApplicationException('Illegal use of callback or two-way binding in dialog-hosted component');
            // }
            //
            // // internal settings
            // dialogSettings.controller = DialogService.dialogInjections.concat(dialogComponent.$inject);
            // dialogSettings.controller.push(DialogService.dialogController);
            // dialogSettings.template = `<div id="Safe.System.Dialog">${dialogComponent.template}</div>`;
            // dialogSettings.resolve = { __S_componentClass: dialogComponent, __S_bindingData: bindingData };
            //
            // let instance = this.modalService.open(dialogSettings);
            //
            // // TODO: Return our own promise fulfilling type?
            //
            // this.openInstances.push(instance);
            //
            // instance.result.then(() => {
            //     let h = 0;
            // });
            // instance.result.then(() => {
            //     let i = 0;
            // });
            // instance.closed.then(() => {
            //     let j = 0;
            //     this.openInstances = _.without(this.openInstances, instance);
            // });
            // instance.result.catch(() => {
            //     let k = 0;
            // });
            //
            // return instance;
        }

        private static dialogInjections = ['$uibModalInstance', '__S_componentClass', '__S_bindingData'];
        private static dialogController(instance: bts.IModalServiceInstance, componentClass: IComponentClass, bindings: INgBinding, ...addlArgs: any[]): Component {
            let newComponent = Object.create(componentClass.prototype) as Component;

            // TODO: Check to see binding data matches component's bindings

            // assign into new object before ctor is called
            _.assign(newComponent, bindings);
            (<any>newComponent)['__S_dialogInstance'] = instance;  // private member

            let result = componentClass.apply(newComponent, addlArgs);

            return (result !== null && _.isObject(result)) ? result : newComponent;
        }

        private openInstances: bts.IModalServiceInstance[] = [];
    }

    export enum DialogResult {
        Unknown,
        OK,
        Cancel,
        Close,
    }

}
namespace Safe.System {
    import bootstrap = angular.ui.bootstrap;
    class ExceptionHandler {
        constructor(private $injector: { get<T>(i:string): T }) {
            // NOTE: IMPORTANT. It's tempting to get the services needed here, but it's too early.
            // NOTE:    wait until services are needed to get them from injector
        }

        /** The entry point for callbacks from Angular when an unhandled exception occurs */
        public handle(error: Error, cause?: string) {
            let exception: Exception = error instanceof Exception ? error : new Exception(error);
            exception.cause = cause;

            try {
                this.log(exception);
                this.popupDialog(exception);
            }
            catch (e) {
                // eat it. nothing can be done, and we don't want to cause another callback
                debugger;   // just in case someone is looking - this is Very Bad (tm) if you break here.
            }
        }

        private popupDialog(exception: Exception) {
            if (this.quiet || this.instance || !exception.viewer)
                return;

            let $modal = this.$injector.get<bootstrap.IModalService>('$uibModal');
            let settings: bootstrap.IModalSettings = {
                template: exception.viewer.template,
                controller: exception.viewer,
                controllerAs: '$ctrl',
                windowClass: 'errormodal',
                resolve: {
                    error: exception
                }
            };

            // TODO: ASYNC eval? Digest cycle could be befouled by this...
            this.instance = $modal.open(settings);
            this.instance.result.then((quiet: boolean) => {
                this.quiet = quiet;
            })
            this.instance.closed.then(() => {
                this.instance = null;
            });
        }

        private log(exception: Exception) {
            // NOTE: This code *specifically* uses as low level as practical to avoid
            // NOTE:    causing additional unexpected exceptions. That means no
            // NOTE:    lookups, no additional services other than angular, etc.

            // log to local logging service (aka, console)
            let $log = this.$injector.get<ng.ILogService>('$log');
            $log.error(exception);

            // no reason to try to log to a dead server
            if (exception instanceof ServerUnavailableException)
                return;

            // log to server - maybe
            let mode = this.safeModule.mode;
            if (mode.reportExceptions) {
                let post = {
                    method: 'POST',
                    url: 'api/diagnostics/reportException',
                    data: {
                        clientTime: new Date(),
                        message: exception.message,
                        information: exception
                    }
                };

                try {
                    this.$http(post).then((response: any) => {
                        // for debugging only; the server just returns 200
                        let data = response.data;
                        let code = response.status;
                    }).catch(() => {
                    });
                }
                catch (e) {
                    // eat it. nothing can be done
                    debugger;   // TODO: if this happens, it's prolly server down
                }
            }
        }

        // NOTE:
        // NOTE: IMPORTANT!!!!
        // NOTE:
        /// NOTE: Change this value to true to turn off exception dialogs UI
        private quiet: boolean = false;
        private instance: bootstrap.IModalServiceInstance;

        private _$http: ng.IHttpService;
        public get $http(): ng.IHttpService {
            if (!this._$http) {
                this._$http = this.$injector.get<ng.IHttpService>('$http');
            }
            return this._$http;
        }

        private _safeModule: Module;
        public get safeModule(): Module {
            if (!this._safeModule) {
                this._safeModule = this.$injector.get<Module>('safeModule');
            }
            return this._safeModule;
        }
    }


    // NOTE: This must be a plain old factory function because that's how the exception handler expects to be registered
    // NOTE: Because this handler uses other UI services, we must avoid circular dependencies thru $injector
    createExceptionHandler.$inject = ['$injector'];
    export function createExceptionHandler($injector: { get<T>(i:string): T }): ng.IExceptionHandlerService {
        let handler = new ExceptionHandler($injector);   // the one and only ExceptionHandler
        return handler.handle.bind(handler);  // handing out reference to object's method (bound to object) ensures object isn't GC'ed
    }

}

// https://www.microsoft.com/en-us/download/confirmation.aspx?id=48593

namespace Safe {
    export namespace Product {
        export namespace Variation {
            export const placeholder = 0;
        }
    }
}

namespace Safe.System {
    interface IComponentMap {
        [angularName: string]: IComponentClass;
    }

    interface IComponentMaps {
        library: IComponentMap;
        baseline: IComponentMap;
        variation: IComponentMap;
        product: IComponentMap;
    }

    export class Module {
        constructor(public mode: Mode, dependencies?: string[]) {
            this._module = angular.module(mode.appName, dependencies);
            this.registerRuntime();

            this.module.constant('safeModule', this);
            this.module.factory('$exceptionHandler', Safe.System.createExceptionHandler);
            this.module.service(this.interceptorName, Safe.System.HttpInterceptor);
            this.module.service('dialogService', Safe.System.DialogService);

            // TODO: Ugly to add library directives "by hand", but until if/when we support them in the
            // TODO: loader, this will have to do. These should be ultra-rare, anyway
            if (Safe.Product.Library) {
                if (Safe.Product.Library.Table && Safe.Product.Library.Table.SafeDataRow) {
                    this.module.directive('safeDataRow', Safe.Product.Library.Table.SafeDataRow);
                }
            }
        }

        public get componentMaps(): IComponentMaps {
            if (!this.maps) {
                let library = this.getLibrary();
                let baseline = this.getBaseline();
                let variation = this.getVariation();

                // TODO: IMPORTANT!!!
                // TODO:    ERROR / SANITY CHECKS - e.g accidental library overwrite

                let product = _.assign({}, library, baseline, variation);

                this.maps = {
                    library: library,
                    baseline: baseline,
                    variation: variation,
                    product: product
                };
            }
            return this.maps;
        }

        private _module: ng.IModule;
        public get module(): ng.IModule {
            return this._module;
        }

        private _interceptorName = randomId('safeHttpInterceptor');
        public get interceptorName() {
            return this._interceptorName;
        }

        private _views: ViewClasses;
        public get views(): ViewClasses {
            if (_.isUndefined(this._views)) {
                if (Safe.Application) {
                    let allFunctions = _.map(_.functions(Safe.Application), (fnName) => (<any>Safe.Application)[fnName]);
                    this._views = _.filter(allFunctions, func => func.prototype instanceof View && func.ngName !== View.ngName);
                }
                else {
                    this._views = [];
                }
            }
            return this._views;
        }

        public config(inlineAnnotatedFunction: any[] | Object): ng.IModule {
            return this.module.config(inlineAnnotatedFunction);
        }

        // TODO: There are many, many things that can be done during registration. Here's a few:
        // *) Route all controllers thru our own "constructor" to add/remove injections
        // *) Add/remove bindings
        // *) Add/change template (or templateUrl)

        // TODO: Experimental
        // TODO: Use these to thunk the constructor and get injections
        // private static magic(ctor: IComponentClass): Function {
        //     let newFunc: Function;
        //
        //     eval(`newFunc = ${Module.creator.toString()}`);
        //     for (let key in Module.creator)
        //         newFunc[key] = Module.creator[key];
        //
        //     newFunc.$inject = ['$element'].concat(ctor.$inject);
        //
        //     return newFunc.bind(ctor);
        // }
        //
        // // $inject = [$element, blah, blah, blah]
        // private static creator($element, ...args: any[]): any {
        //     debugger;
        //     let me: Function = this;
        //     let newObj = Object.create(me);
        //     me.apply(newObj, args);
        //     newObj.$element = $element;
        //     return newObj;
        // }

        public service(name: string, serviceConstructor: Function): ng.IModule {
            return this.module.service(name, serviceConstructor);
        }

        private maps: IComponentMaps;

        private registerRuntime() {
            // TODO: Support old style? I say NOOOO!!! So, if there are bindings here now, vomit loudly

            // register components
            let product = this.componentMaps.product;
            _.each(_.values(product), (componentClass: IComponentClass) => {
                let componentName = componentClass.ngName;

                if (componentName.toLowerCase().indexOf('safe') !== 0) {
                    throw new FoundationException(`Component name MUST begin with 'safe'. Component: ${componentName}`);
                }

                let bindings: INgBinding =
                    componentClass.bindings ||
                    Binding.ToNgBinding(componentClass.GetAllBindings());

                // TODO: Thunk the ctor to grab my injections
                let controller: Function = componentClass; //(componentClass.ngName === 'safeInput') ? Module.magic(componentClass) : componentClass;

                // TODO: Requiring a safeGroup isn't prolly what I want. I really
                // TODO:   want to get to the view from a component. There's a better way
                let options: ng.IComponentOptions = {
                    controller: controller,
                    controllerAs: '$ctrl',
                    template: componentClass.template,
                    bindings: bindings,
                    transclude: !!componentClass.transclude,
                    require: _.assign({}, {group: '?^^safeGroup'}, componentClass.require),
                };
                this.module.component(componentName, options);
            });
        }

        private getComponentMap(ns: AnyObj, currentMap: IComponentMap = {}): IComponentMap {
            let allFunctions = _.map(_.functions(ns), (fnName) => ns[fnName]);
            let components = _.filter(allFunctions, func => (func.prototype instanceof Component) && (func.ngName !== Component.ngName));
            let mappedComponents = _.assign(currentMap, _.indexBy(components, 'ngName'));

            // load sub namespaces, too
            let allObjects = _.filter(ns, (thing) => _.isObject(thing) && !_.isFunction(thing) && !_.isArray(thing));
            _.each(allObjects, (subNamespace) => {
                return this.getComponentMap(subNamespace, mappedComponents);
            });

            return mappedComponents;
        }

        private getLibrary(): IComponentMap {
            let libraryRoot: any = Safe && Safe.Product && Safe.Product.Library;
            if (!libraryRoot) {
                return {};
            }

            return this.getComponentMap(libraryRoot);
        }

        private getBaseline(): IComponentMap {
            let productRoot: any = Safe && Safe.Product;
            if (!productRoot) {
                return {};
            }

            return this.getComponentMap(productRoot);
        }

        private getVariation(): IComponentMap {
            let variationRoot: any = Safe && Safe.Product && Safe.Product.Variation;
            if (!variationRoot) {
                return {};
            }

            let variationNames = _.keys(variationRoot);
            if (!variationNames.length) {
                return {};
            }

            // note; SANITY CHECK for unexpected stuff
            if (variationNames.length > 1) {
                throw new Error(`Unexpected number (${variationNames.length}) of variations loaded.`);
            }
            return this.getComponentMap((<any>Safe.Product.Variation)[variationNames[0]]);
        }

        //endregion

    }
}

var Safe;
(function (Safe) {
    var System;
    (function (System) {
        var ExceptionHandler = (function () {
            function ExceptionHandler($injector) {
                this.$injector = $injector;
                // NOTE:
                // NOTE: IMPORTANT!!!!
                // NOTE:
                /// NOTE: Change this value to true to turn off exception dialogs UI
                this.quiet = false;
                // NOTE: IMPORTANT. It's tempting to get the services needed here, but it's too early.
                // NOTE:    wait until services are needed to get them from injector
            }
            /** The entry point for callbacks from Angular when an unhandled exception occurs */
            ExceptionHandler.prototype.handle = function (error, cause) {
                var exception = error instanceof System.Exception ? error : new System.Exception(error);
                exception.cause = cause;
                try {
                    this.log(exception);
                    this.popupDialog(exception);
                }
                catch (e) {
                    // eat it. nothing can be done, and we don't want to cause another callback
                    debugger; // just in case someone is looking - this is Very Bad (tm) if you break here.
                }
            };
            ExceptionHandler.prototype.popupDialog = function (exception) {
                var _this = this;
                if (this.quiet || this.instance || !exception.viewer)
                    return;
                var $modal = this.$injector.get('$uibModal');
                var settings = {
                    template: exception.viewer.template,
                    controller: exception.viewer,
                    controllerAs: '$ctrl',
                    windowClass: 'errormodal',
                    resolve: {
                        error: exception
                    }
                };
                // TODO: ASYNC eval? Digest cycle could be befouled by this...
                this.instance = $modal.open(settings);
                this.instance.result.then(function (quiet) {
                    _this.quiet = quiet;
                });
                this.instance.closed.then(function () {
                    _this.instance = null;
                });
            };
            ExceptionHandler.prototype.log = function (exception) {
                // NOTE: This code *specifically* uses as low level as practical to avoid
                // NOTE:    causing additional unexpected exceptions. That means no
                // NOTE:    lookups, no additional services other than angular, etc.
                // log to local logging service (aka, console)
                var $log = this.$injector.get('$log');
                $log.error(exception);
                // no reason to try to log to a dead server
                if (exception instanceof System.ServerUnavailableException)
                    return;
                // log to server - maybe
                var mode = this.safeModule.mode;
                if (mode.reportExceptions) {
                    var post = {
                        method: 'POST',
                        url: 'api/diagnostics/reportException',
                        data: {
                            clientTime: new Date(),
                            message: exception.message,
                            information: exception
                        }
                    };
                    try {
                        this.$http(post).then(function (response) {
                            // for debugging only; the server just returns 200
                            var data = response.data;
                            var code = response.status;
                        }).catch(function () {
                        });
                    }
                    catch (e) {
                        // eat it. nothing can be done
                        debugger; // TODO: if this happens, it's prolly server down
                    }
                }
            };
            Object.defineProperty(ExceptionHandler.prototype, "$http", {
                get: function () {
                    if (!this._$http) {
                        this._$http = this.$injector.get('$http');
                    }
                    return this._$http;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ExceptionHandler.prototype, "safeModule", {
                get: function () {
                    if (!this._safeModule) {
                        this._safeModule = this.$injector.get('safeModule');
                    }
                    return this._safeModule;
                },
                enumerable: true,
                configurable: true
            });
            return ExceptionHandler;
        }());
        // NOTE: This must be a plain old factory function because that's how the exception handler expects to be registered
        // NOTE: Because this handler uses other UI services, we must avoid circular dependencies thru $injector
        createExceptionHandler.$inject = ['$injector'];
        function createExceptionHandler($injector) {
            var handler = new ExceptionHandler($injector); // the one and only ExceptionHandler
            return handler.handle.bind(handler); // handing out reference to object's method (bound to object) ensures object isn't GC'ed
        }
        System.createExceptionHandler = createExceptionHandler;
    })(System = Safe.System || (Safe.System = {}));
})(Safe || (Safe = {}));
// https://www.microsoft.com/en-us/download/confirmation.aspx?id=48593
//# sourceMappingURL=exceptionHandler.js.map
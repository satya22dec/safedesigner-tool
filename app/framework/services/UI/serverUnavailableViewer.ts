/**
 * Created by mtelb on 5/10/17.
 */
namespace Safe.System.UI {
    import bootstrap = angular.ui.bootstrap;
    export class ServerUnavailableViewer {
        public static $inject = ['$uibModalInstance', 'error'];
        constructor(private instance: bootstrap.IModalServiceInstance, private error: Exception) {

        }

        public close() {
            this.instance.close();
        }

        static template: string =
`
            <div class="modal-header">
                <h2 style="margin: 1em; text-align: center">Nobody's home...<span style="font-family: 'Segoe UI Emoji', 'Segoe UI Symbol', sans-serif">&#9785;</span></h2>
                <hr/>
            </div>
            <div class="modal-body">
                <p>We're trying to contact the server, but it's unavailable. Please try refreshing your browser or logging out and back in. If the problem persists, contact your administrator.</p>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" ng-click="$ctrl.close()">Close</button>
            </div>
`;
    }

}

/**
 * Created by mtelb on 5/10/17.
 */
namespace Safe.System.UI {
    import bootstrap = angular.ui.bootstrap;
    export class ServerValidationExceptionViewer {
        public static $inject = ['$uibModalInstance', 'error'];
        constructor(private instance: bootstrap.IModalServiceInstance, private exception: ServerValidationException) {
            this.errors = [];
            if (exception && exception.errors)
                this.errors = exception.errors;
        }

        private errors:any;

        public close() {
            this.instance.close();
        }

        static template: string =
`
            <div class="modal-header">
                <h2 style="margin: 1em; text-align: center">Following Validation Error(s) Occurred !</h2>
                <hr/>
            </div>
            <div class="modal-body">
                <ul>
                    <li ng-repeat="error in $ctrl.errors">
                        <p>{{ error.message | translate }}</p>
                    </li>
                </ul>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" ng-click="$ctrl.close()">Close</button>
            </div>
`;
    }

}

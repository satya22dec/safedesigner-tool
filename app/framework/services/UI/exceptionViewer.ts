/**
 * Created by mtelb on 5/10/17.
 */
namespace Safe.System.UI {
    import bootstrap = angular.ui.bootstrap;

    export interface IExceptionViewer {
        new (instance: bootstrap.IModalServiceInstance, exception: Exception, ...args: any[]): any;
        template: string;
    }

    /** Basic view/controller for displaying errors */
    export class ExceptionViewer {
        public static $inject = ['$uibModalInstance', 'error', '$document'];

        constructor(private instance: bootstrap.IModalServiceInstance, private error: Exception, $document: ng.IDocumentService) {
            this.document = $document[0];
        }

        private copyDetails() {
            let textArea = this.document.getElementById('details') as HTMLTextAreaElement;
            textArea.focus();
            textArea.setSelectionRange(0, 0x7fff);
            this.document.execCommand('copy');
        }

        private debug() {
            debugger;
        }

        public close() {
            this.instance.close(this.quiet);
        }

        private document: HTMLDocument;
        private quiet: boolean = false;

        private showDetails = false;
        private showStack = false;

        public static template =
            `
            <style>
            textarea {
                font-family: "Fira Code", monospace;
                font-size: 10pt;
                white-space: nowrap;
                overflow: auto;
            }
            a.showDeets {
                display: block;
            }
            a.showDeets:hover {
                text-decoration: none;
            }            
            </style>
            <div class="modal-header">
                <h2 style="margin: 1em; text-align: center">OOPS! Something went wrong...<span style="font-family: 'Segoe UI Emoji', 'Segoe UI Symbol', sans-serif">&#9785;</span></h2>
                <h3 style="color: darkred">{{$ctrl.error.name}}</h3>
                <h4 style="color: darkred">{{$ctrl.error.message}}</h4>
            </div>
            <div class="modal-body">
                <a class="showDeets" ng-click="$ctrl.showDetails = !$ctrl.showDetails">Details</a>
                <textarea ng-show="$ctrl.showDetails" id="details" rows="5" spellcheck="false" class="form-control">Safe.Exception = {{$ctrl.error | json}}</textarea>
                <div style="display: block; margin: 0.5em 0"></div>
                <a class="showDeets" ng-click="$ctrl.showStack = !$ctrl.showStack">Stack</a>
                <textarea ng-show="$ctrl.showStack" id="stack" rows="5" spellcheck="false" class="form-control">{{$ctrl.error.stack}}</textarea>
                
                <input id="quiet" style="margin-right: 5px; vertical-align: middle" type="checkbox" ng-model="$ctrl.quiet"/>
                <label for="quiet" class="form-label" style="margin-top: 10px">Don't show again</label>
            </div>
            <div class="modal-footer">
                <button class="btn" ng-click="$ctrl.copyDetails()">Copy</button>
                <button class="btn" ng-click="$ctrl.debug()">Debug</button>
                <button class="btn btn-primary" ng-click="$ctrl.close()">Close</button>
            </div>
            `;

    }
}

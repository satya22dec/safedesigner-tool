/**
 * Created by mtelb on 5/10/17.
 */
var Safe;
(function (Safe) {
    var System;
    (function (System) {
        var UI;
        (function (UI) {
            /** Basic view/controller for displaying errors */
            var ExceptionViewer = (function () {
                function ExceptionViewer(instance, error, $document) {
                    this.instance = instance;
                    this.error = error;
                    this.quiet = false;
                    this.showDetails = false;
                    this.showStack = false;
                    this.document = $document[0];
                }
                ExceptionViewer.prototype.copyDetails = function () {
                    var textArea = this.document.getElementById('details');
                    textArea.focus();
                    textArea.setSelectionRange(0, 0x7fff);
                    this.document.execCommand('copy');
                };
                ExceptionViewer.prototype.debug = function () {
                    debugger;
                };
                ExceptionViewer.prototype.close = function () {
                    this.instance.close(this.quiet);
                };
                return ExceptionViewer;
            }());
            ExceptionViewer.$inject = ['$uibModalInstance', 'error', '$document'];
            ExceptionViewer.template = "\n            <style>\n            textarea {\n                font-family: \"Fira Code\", monospace;\n                font-size: 10pt;\n                white-space: nowrap;\n                overflow: auto;\n            }\n            a.showDeets {\n                display: block;\n            }\n            a.showDeets:hover {\n                text-decoration: none;\n            }            \n            </style>\n            <div class=\"modal-header\">\n                <h2 style=\"margin: 1em; text-align: center\">OOPS! Something went wrong...<span style=\"font-family: 'Segoe UI Emoji', 'Segoe UI Symbol', sans-serif\">&#9785;</span></h2>\n                <h3 style=\"color: darkred\">{{$ctrl.error.name}}</h3>\n                <h4 style=\"color: darkred\">{{$ctrl.error.message}}</h4>\n            </div>\n            <div class=\"modal-body\">\n                <a class=\"showDeets\" ng-click=\"$ctrl.showDetails = !$ctrl.showDetails\">Details</a>\n                <textarea ng-show=\"$ctrl.showDetails\" id=\"details\" rows=\"5\" spellcheck=\"false\" class=\"form-control\">Safe.Exception = {{$ctrl.error | json}}</textarea>\n                <div style=\"display: block; margin: 0.5em 0\"></div>\n                <a class=\"showDeets\" ng-click=\"$ctrl.showStack = !$ctrl.showStack\">Stack</a>\n                <textarea ng-show=\"$ctrl.showStack\" id=\"stack\" rows=\"5\" spellcheck=\"false\" class=\"form-control\">{{$ctrl.error.stack}}</textarea>\n                \n                <input id=\"quiet\" style=\"margin-right: 5px; vertical-align: middle\" type=\"checkbox\" ng-model=\"$ctrl.quiet\"/>\n                <label for=\"quiet\" class=\"form-label\" style=\"margin-top: 10px\">Don't show again</label>\n            </div>\n            <div class=\"modal-footer\">\n                <button class=\"btn\" ng-click=\"$ctrl.copyDetails()\">Copy</button>\n                <button class=\"btn\" ng-click=\"$ctrl.debug()\">Debug</button>\n                <button class=\"btn btn-primary\" ng-click=\"$ctrl.close()\">Close</button>\n            </div>\n            ";
            UI.ExceptionViewer = ExceptionViewer;
        })(UI = System.UI || (System.UI = {}));
    })(System = Safe.System || (Safe.System = {}));
})(Safe || (Safe = {}));
//# sourceMappingURL=exceptionViewer.js.map
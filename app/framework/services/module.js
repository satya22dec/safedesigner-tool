var Safe;
(function (Safe) {
    var Product;
    (function (Product) {
        var Variation;
        (function (Variation) {
            Variation.placeholder = 0;
        })(Variation = Product.Variation || (Product.Variation = {}));
    })(Product = Safe.Product || (Safe.Product = {}));
})(Safe || (Safe = {}));
(function (Safe) {
    var System;
    (function (System) {
        var Module = (function () {
            function Module(mode, dependencies) {
                this.mode = mode;
                this._interceptorName = Safe.randomId('safeHttpInterceptor');
                this._module = angular.module(mode.appName, dependencies);
                this.registerRuntime();
                this.module.constant('safeModule', this);
                this.module.factory('$exceptionHandler', Safe.System.createExceptionHandler);
                this.module.service(this.interceptorName, Safe.System.HttpInterceptor);
                this.module.service('dialogService', Safe.System.DialogService);
                // TODO: Ugly to add library directives "by hand", but until if/when we support them in the
                // TODO: loader, this will have to do. These should be ultra-rare, anyway
                if (Safe.Product.Library) {
                    if (Safe.Product.Library.Table && Safe.Product.Library.Table.SafeDataRow) {
                        this.module.directive('safeDataRow', Safe.Product.Library.Table.SafeDataRow);
                    }
                }
            }
            Object.defineProperty(Module.prototype, "componentMaps", {
                get: function () {
                    if (!this.maps) {
                        var library = this.getLibrary();
                        var baseline = this.getBaseline();
                        var variation = this.getVariation();
                        // TODO: IMPORTANT!!!
                        // TODO:    ERROR / SANITY CHECKS - e.g accidental library overwrite
                        var product = _.assign({}, library, baseline, variation);
                        this.maps = {
                            library: library,
                            baseline: baseline,
                            variation: variation,
                            product: product
                        };
                    }
                    return this.maps;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Module.prototype, "module", {
                get: function () {
                    return this._module;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Module.prototype, "interceptorName", {
                get: function () {
                    return this._interceptorName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Module.prototype, "views", {
                get: function () {
                    if (_.isUndefined(this._views)) {
                        if (Safe.Application) {
                            var allFunctions = _.map(_.functions(Safe.Application), function (fnName) { return Safe.Application[fnName]; });
                            this._views = _.filter(allFunctions, function (func) { return func.prototype instanceof Safe.View && func.ngName !== Safe.View.ngName; });
                        }
                        else {
                            this._views = [];
                        }
                    }
                    return this._views;
                },
                enumerable: true,
                configurable: true
            });
            Module.prototype.config = function (inlineAnnotatedFunction) {
                return this.module.config(inlineAnnotatedFunction);
            };
            // TODO: There are many, many things that can be done during registration. Here's a few:
            // *) Route all controllers thru our own "constructor" to add/remove injections
            // *) Add/remove bindings
            // *) Add/change template (or templateUrl)
            // TODO: Experimental
            // TODO: Use these to thunk the constructor and get injections
            // private static magic(ctor: IComponentClass): Function {
            //     let newFunc: Function;
            //
            //     eval(`newFunc = ${Module.creator.toString()}`);
            //     for (let key in Module.creator)
            //         newFunc[key] = Module.creator[key];
            //
            //     newFunc.$inject = ['$element'].concat(ctor.$inject);
            //
            //     return newFunc.bind(ctor);
            // }
            //
            // // $inject = [$element, blah, blah, blah]
            // private static creator($element, ...args: any[]): any {
            //     debugger;
            //     let me: Function = this;
            //     let newObj = Object.create(me);
            //     me.apply(newObj, args);
            //     newObj.$element = $element;
            //     return newObj;
            // }
            Module.prototype.service = function (name, serviceConstructor) {
                return this.module.service(name, serviceConstructor);
            };
            Module.prototype.registerRuntime = function () {
                // TODO: Support old style? I say NOOOO!!! So, if there are bindings here now, vomit loudly
                var _this = this;
                // register components
                var product = this.componentMaps.product;
                _.each(_.values(product), function (componentClass) {
                    var componentName = componentClass.ngName;
                    if (componentName.toLowerCase().indexOf('safe') !== 0) {
                        throw new System.FoundationException("Component name MUST begin with 'safe'. Component: " + componentName);
                    }
                    var bindings = componentClass.bindings ||
                        Safe.Binding.ToNgBinding(componentClass.GetAllBindings());
                    // TODO: Thunk the ctor to grab my injections
                    var controller = componentClass; //(componentClass.ngName === 'safeInput') ? Module.magic(componentClass) : componentClass;
                    // TODO: Requiring a safeGroup isn't prolly what I want. I really
                    // TODO:   want to get to the view from a component. There's a better way
                    var options = {
                        controller: controller,
                        controllerAs: '$ctrl',
                        template: componentClass.template,
                        bindings: bindings,
                        transclude: !!componentClass.transclude,
                        require: _.assign({}, { group: '?^^safeGroup' }, componentClass.require),
                    };
                    _this.module.component(componentName, options);
                });
            };
            Module.prototype.getComponentMap = function (ns, currentMap) {
                var _this = this;
                if (currentMap === void 0) { currentMap = {}; }
                var allFunctions = _.map(_.functions(ns), function (fnName) { return ns[fnName]; });
                var components = _.filter(allFunctions, function (func) { return (func.prototype instanceof Safe.Component) && (func.ngName !== Safe.Component.ngName); });
                var mappedComponents = _.assign(currentMap, _.indexBy(components, 'ngName'));
                // load sub namespaces, too
                var allObjects = _.filter(ns, function (thing) { return _.isObject(thing) && !_.isFunction(thing) && !_.isArray(thing); });
                _.each(allObjects, function (subNamespace) {
                    return _this.getComponentMap(subNamespace, mappedComponents);
                });
                return mappedComponents;
            };
            Module.prototype.getLibrary = function () {
                var libraryRoot = Safe && Safe.Product && Safe.Product.Library;
                if (!libraryRoot) {
                    return {};
                }
                return this.getComponentMap(libraryRoot);
            };
            Module.prototype.getBaseline = function () {
                var productRoot = Safe && Safe.Product;
                if (!productRoot) {
                    return {};
                }
                return this.getComponentMap(productRoot);
            };
            Module.prototype.getVariation = function () {
                var variationRoot = Safe && Safe.Product && Safe.Product.Variation;
                if (!variationRoot) {
                    return {};
                }
                var variationNames = _.keys(variationRoot);
                if (!variationNames.length) {
                    return {};
                }
                // note; SANITY CHECK for unexpected stuff
                if (variationNames.length > 1) {
                    throw new Error("Unexpected number (" + variationNames.length + ") of variations loaded.");
                }
                return this.getComponentMap(Safe.Product.Variation[variationNames[0]]);
            };
            return Module;
        }());
        System.Module = Module;
    })(System = Safe.System || (Safe.System = {}));
})(Safe || (Safe = {}));
//# sourceMappingURL=module.js.map
/// <reference path="../../typings/index.d.ts" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Safe;
(function (Safe) {
    var System;
    (function (System) {
        // TODO: Sooo much more
        var Debug = (function () {
            function Debug() {
            }
            Debug.stop = function (msg) {
                msg && console.warn(msg);
                Debug.stopIf();
            };
            Debug.stopIf = function (predicate) {
                if (predicate === void 0) { predicate = function () { return true; }; }
                if (predicate()) {
                    debugger;
                }
            };
            Debug.countdown = function (counter) {
                if (counter === void 0) { counter = new Counter(); }
                if (counter.count) {
                    --counter.count;
                    Debug.stop();
                }
            };
            return Debug;
        }());
        System.Debug = Debug;
        var Counter = (function () {
            function Counter(count) {
                if (count === void 0) { count = 1; }
                this.count = count;
            }
            return Counter;
        }());
        System.Counter = Counter;
        var Assert = (function () {
            function Assert() {
            }
            Assert.isDefined = function (v) {
                if (_.isUndefined(v)) {
                    throw new AssertionException('Assert.isDefined');
                }
            };
            Assert.isNotNull = function (v) {
                if (v === null) {
                    throw new AssertionException('Assert.isNotNull');
                }
            };
            Assert.isNull = function (v) {
                if (v !== null) {
                    throw new AssertionException('Assert.isNull');
                }
            };
            Assert.hasValue = function (v) {
                Assert.isDefined(v);
                Assert.isNotNull(v);
            };
            Assert.isTrue = function (v) {
                if (!v) {
                    throw new AssertionException('Assert.isTrue');
                }
            };
            Assert.isFalse = function (v) {
                if (v) {
                    throw new AssertionException('Assert.isFalse');
                }
            };
            Assert.isNotEmpty = function (v) {
                Assert.isDefined(v);
                if (v.length === 0) {
                    throw new AssertionException('Assert.isNotEmpty');
                }
            };
            return Assert;
        }());
        System.Assert = Assert;
        var Exception = (function () {
            function Exception(message) {
                if (message === void 0) { message = ''; }
                //region -- additional exception cause information
                this._cause = null;
                if (Exception.stopOnThrow) {
                    debugger;
                }
                if (message instanceof Error) {
                    this.error = message;
                }
                else {
                    this.error = new Error(_.isString(message) ? message : message.message || message.statusText || message.errorDescription || 'Unknown Exception');
                    this.cause = message;
                }
                this.error.name = this.constructor.name;
                this._stackFrames = _.map(this.error.stack.split(/\n/), function (padded) { return padded.trim(); });
            }
            Exception.prototype.toJSON = function () {
                return _.omit(this, ['_cause', 'stack', '_stackFrames', 'viewer']);
            };
            Object.defineProperty(Exception.prototype, "cause", {
                get: function () {
                    return this._cause;
                },
                set: function (newVal) {
                    // don't allow resetting the cause. This is a set-once property.
                    this._cause = this._cause || newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Exception.prototype, "viewer", {
                get: function () {
                    if (!this._viewer) {
                        this._viewer = System.UI.ExceptionViewer;
                    }
                    return this._viewer;
                },
                set: function (newVal) {
                    this._viewer = newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Exception.prototype, "name", {
                get: function () {
                    return this.error.name;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Exception.prototype, "message", {
                get: function () {
                    return this.error.message;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Exception.prototype, "stack", {
                get: function () {
                    return this._stackFrames.join('\n');
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Exception.prototype, "stackFrames", {
                get: function () {
                    return this._stackFrames;
                },
                enumerable: true,
                configurable: true
            });
            return Exception;
        }());
        Exception.stopOnThrow = false;
        System.Exception = Exception;
        /** Type representing system exceptions (such as Http errors) */
        var SystemException = (function (_super) {
            __extends(SystemException, _super);
            function SystemException(message) {
                return _super.call(this, message) || this;
            }
            return SystemException;
        }(Exception));
        System.SystemException = SystemException;
        /** Type representing Http exceptions */
        var HttpException = (function (_super) {
            __extends(HttpException, _super);
            function HttpException(rejection) {
                return _super.call(this, rejection) || this;
            }
            Object.defineProperty(HttpException.prototype, "statusCode", {
                // addl properties
                get: function () {
                    var rejection = this.cause;
                    return rejection.status;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(HttpException.prototype, "badUrl", {
                get: function () {
                    var rejection = this.cause;
                    return rejection.config && rejection.config.url;
                },
                enumerable: true,
                configurable: true
            });
            return HttpException;
        }(SystemException));
        System.HttpException = HttpException;
        var ServerUnavailableException = (function (_super) {
            __extends(ServerUnavailableException, _super);
            function ServerUnavailableException(rejection) {
                var _this = _super.call(this, rejection) || this;
                _this.viewer = System.UI.ServerUnavailableViewer;
                return _this;
            }
            return ServerUnavailableException;
        }(HttpException));
        System.ServerUnavailableException = ServerUnavailableException;
        var ServerValidationException = (function (_super) {
            __extends(ServerValidationException, _super);
            function ServerValidationException(rejection) {
                var _this = _super.call(this, rejection) || this;
                _this.errors = [];
                _this.viewer = System.UI.ServerValidationExceptionViewer;
                if (rejection && rejection.data) {
                    _this.addToErrorList(rejection);
                }
                return _this;
            }
            ServerValidationException.prototype.addToErrorList = function (rejection) {
                Assert.hasValue(rejection);
                if (rejection && rejection.data) {
                    for (var key in rejection.data) {
                        var errorMessage = {};
                        errorMessage['message'] = rejection.data[key][0];
                        errorMessage[key] = rejection.data[key][0];
                        this.errors.push(errorMessage);
                    }
                }
            };
            return ServerValidationException;
        }(HttpException));
        System.ServerValidationException = ServerValidationException;
        /** Type representing application exceptions (such as logical errors) */
        var ApplicationException = (function (_super) {
            __extends(ApplicationException, _super);
            function ApplicationException(message) {
                return _super.call(this, message) || this;
            }
            return ApplicationException;
        }(Exception));
        System.ApplicationException = ApplicationException;
        var FoundationException = (function (_super) {
            __extends(FoundationException, _super);
            function FoundationException(message) {
                return _super.call(this, message) || this;
            }
            return FoundationException;
        }(Exception));
        System.FoundationException = FoundationException;
        /** Type representing exceptions from the Angular framework */
        var AngularException = (function (_super) {
            __extends(AngularException, _super);
            function AngularException(message) {
                return _super.call(this, message) || this;
            }
            return AngularException;
        }(Exception));
        System.AngularException = AngularException;
        /** Type representing application validation exceptions */
        var ValidationException = (function (_super) {
            __extends(ValidationException, _super);
            function ValidationException(message) {
                return _super.call(this, message) || this;
            }
            return ValidationException;
        }(ApplicationException));
        System.ValidationException = ValidationException;
        /** Type representing exception from running code which is not implemented */
        var NotImplementedException = (function (_super) {
            __extends(NotImplementedException, _super);
            function NotImplementedException(message) {
                return _super.call(this, message) || this;
            }
            return NotImplementedException;
        }(ApplicationException));
        System.NotImplementedException = NotImplementedException;
        /** Type representing exception from a failed assertion */
        var AssertionException = (function (_super) {
            __extends(AssertionException, _super);
            function AssertionException(message) {
                return _super.call(this, message) || this;
            }
            return AssertionException;
        }(ApplicationException));
        System.AssertionException = AssertionException;
        /* Debugging tool - developer use only */
        var Break = (function () {
            function Break() {
            }
            Break.stop = function () {
                debugger;
            };
            return Break;
        }());
        System.Break = Break;
    })(System = Safe.System || (Safe.System = {}));
})(Safe || (Safe = {}));
//# sourceMappingURL=_exception.js.map
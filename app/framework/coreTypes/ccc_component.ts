namespace Safe {
    /** Represents the component's TypeScript class & related information */
    export interface IComponentClass extends Function {
        new (...args: any[]): Component;
        GetAllBindings(): Bindings;
        $inject: string[];
        ngName: string;
        exBindings: Bindings;
        template: string;
        transclude: boolean | object;
        bindings?: INgBinding;
        require?: StringMap;
    }
    export type ControlType = IComponentClass;

    export interface IExtendedScope extends ng.IScope {
        $$watchers: {exp: string}[];
        $ctrl: Component;
    }

    /** Represents a Safe component */
    export abstract class Component implements ng.IComponentController {
        //region statics for IComponentClass

        static $inject: string[] = [];
        static ngName = '!!-BAADF00D,1';
        static template = '<H1>BAD TEMPLATE</H1>';
        static transclude: boolean|object = false;
        static require: StringMap = {};

        static GetAllBindings(): Bindings {
            return Component.allBindings(this);
        }

        //endregion

        /** Initializes a new instance of this class */
        constructor() {
            this.createSmartVars();

            if (this.idProp.isUndefined) {
                let ctor: IComponentClass = this.constructor as IComponentClass;
                let id = randomId(ctor.ngName);
                this.idProp.setIf(id);
            }
        }

        //region lifecycle management

        /**
         * Called on each controller after all the controllers on an element have been constructed and had their bindings
         * initialized (and before the pre & post linking functions for the directives on this element). This is a good
         * place to put initialization code for your controller.
         */
        $onInit(): void {
        }

        /**
         * Called whenever one-way bindings are updated. The changesObj is a hash whose keys are the names of the bound
         * properties that have changed, and the values are an {@link IChangesObject} object  of the form
         * { currentValue, previousValue, isFirstChange() }. Use this hook to trigger updates within a component such as
         * cloning the bound value to prevent accidental mutation of the outer value.
         */
        $onChanges(changesObj: { [property: string]: ng.IChangesObject }): void {
            changesObj;
        }

        /**
         * Called on a controller when its containing scope is destroyed. Use this hook for releasing external resources,
         * watches and event handlers.
         */
        $onDestroy(): void {
            //this.parent.removeChild(this);
        }

        /**
         * Called after this controller's element and its children have been linked. Similar to the post-link function this
         * hook can be used to set up DOM event handlers and do direct DOM manipulation. Note that child elements that contain
         * templateUrl directives will not have been compiled and linked since they are waiting for their template to load
         * asynchronously and their own compilation and linking has been suspended until that occurs. This hook can be considered
         * analogous to the ngAfterViewInit and ngAfterContentInit hooks in Angular 2. Since the compilation process is rather
         * different in Angular 1 there is no direct mapping and care should be taken when upgrading.
         */
        $postLink(): void {
            //this.discoverChildren();
        }

        //endregion

        //region public interface

        private _$compile: ng.ICompileService;
        protected get $compile() {
            if (this._$compile) {
                this._$compile = angular.injector().get('$compile');
            }
            return this._$compile;
        }

        public seal(ctor: ControlType) {
            let myName = this.constructor.name;
            if (!(this instanceof ctor)) {
                throw new System.FoundationException(`Attempt to derive from a sealed type. Base: ${ctor.name}; Derived: ${myName}`)
            }
        }

        private _allBindings: Bindings;
        public get allBindings(): Bindings {
            if (_.isUndefined(this._allBindings)) {
                this._allBindings = Component.allBindings(this);
            }
            return this._allBindings;
        }

        //endregion

        private createSmartVars() {
            let anyMe = this as AnyObj;
            _.each(this.allBindings, (binding: Binding) => {
                if (binding.smartVarEnabled) {
                    SmartVar.BindTo(binding, this);
                }
                else {
                    if (_.isUndefined(anyMe[binding.name])) {
                        anyMe[binding.name] = binding.defaultValue;
                    }
                    if (binding.required && _.isUndefined(anyMe[binding.name])) {
                        throw new System.FoundationException(`Missing required binding ${binding.toString()}`);
                    }
                }
            }, this);
        }

        //region bindings
        static exBindings: Binding[] = [
            new StaticBinding('id').useSmartVar(),
            new TwoWayBinding('ioValue', VarType.Any).useSmartVar(),

            // TODO: special. rarely (I hope) used binding to expose the controller more easily...
            new TwoWayBinding('ioThis', VarType.Component)
        ];

        // private id: string;
        public readonly idProp: SmartString;
        // private ioValue: any;
        public readonly ioValueProp: SmartAny;

        // special...
        private ioThis:Component = this;

        //endregion

        // region child management
        // TODO: Consider moving this to a mixin type

        public addChild(child: Component): Component {
            if (!_.contains(this.children, child)) {
                this.children.push(child);
                child._parent = this;
            }
            return this;
        }

        public removeChild(child: Component): Component {
            child._parent = null;
            this._children = _.without(this.children, child);
            return this;
        }

        public getChild(id: string): Component {
            return _.find(this.children, child => child.idProp.eq(id));
        }

        /** Default predicate selects all children */
        public getChildren(selector: Predicate<Control> = () => true): Components {
            return _.select(this.children, selector);
        }

        public getChildrenOfType<C extends Control>(type: ControlType): C[] {
            return this.getChildren(c => c instanceof type) as C[];
        }

        private _children: Components;
        private get children(): Components {
            if (_.isUndefined(this._children)) {
                this.discoverChildren();
            }

            return this._children;
        }

        public discoverChildren() {
            this._children = [];
            let jChildren = $(document.getElementById(this.idProp.value)).filter((idx, el) => el.tagName.toLowerCase().indexOf('safe-') === 0);
            jChildren.each((idx, el) => {
                debugger;
                let jElement = $(el);
                let control: Control = jElement.controller(el.tagName.dedasherize());
                control && this.addChild(control);
            });
        }

        private _parent: Component;
        protected get parent() {
            return this._parent;
        }
        //endregion


        //region internal use
        private static allBindings(ctor: any): Bindings {
            ctor = _.isFunction(ctor) ? ctor : ctor.constructor;

            if (ctor.bindings) {    // ugh - old school bindings check
                return [];
            }
            return (function r(cc, ret: any[] = []): any {
                ret = _.uniq(ret.concat(cc.exBindings));
                return (cc.prototype instanceof Component) ? r(Object.getPrototypeOf(cc), ret) : ret;
            })(ctor);
        }
        //endregion
    }
    export type Components = Component[];


}

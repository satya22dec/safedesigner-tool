var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Safe;
(function (Safe) {
    var Product;
    (function (Product) {
        var Exception = Safe.System.Exception;
        var SafeDebug = (function (_super) {
            __extends(SafeDebug, _super);
            function SafeDebug() {
                var _this = _super.call(this) || this;
                Exception.stopOnThrow = true;
                return _this;
            }
            SafeDebug.prototype.$onDestroy = function () {
                Exception.stopOnThrow = false;
            };
            return SafeDebug;
        }(Safe.Control));
        SafeDebug.ngName = 'safeDebugPage';
        SafeDebug.template = "<div id=\"__debugPage\" style=\"display: none; visibility: hidden\"></div>";
        Product.SafeDebug = SafeDebug;
        var ControlGroup = (function (_super) {
            __extends(ControlGroup, _super);
            function ControlGroup() {
                var _this = _super.call(this) || this;
                _this.seal(ControlGroup);
                return _this;
            }
            return ControlGroup;
        }(Safe.Control));
        ControlGroup.ngName = 'safeGroup';
        ControlGroup.transclude = true;
        ControlGroup.template = "<div name=\"{{$ctrl.name}}\" id=\"{{$ctrl.id}}\" ng-show=\"{{$ctrl.inShow}}\"><ng-transclude></ng-transclude></div>";
        Product.ControlGroup = ControlGroup;
    })(Product = Safe.Product || (Safe.Product = {}));
})(Safe || (Safe = {}));
//# sourceMappingURL=fff_component.js.map
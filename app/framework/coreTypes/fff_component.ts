namespace Safe.Product {
    import Exception = Safe.System.Exception;
    export class SafeDebug extends Control {
        static ngName = 'safeDebugPage';
        constructor() {
            super();
            Exception.stopOnThrow = true;
        }

        public $onDestroy() {
            Exception.stopOnThrow = false;
        }

        static template = `<div id="__debugPage" style="display: none; visibility: hidden"></div>`;
    }


    export class ControlGroup extends Control {
        static ngName = 'safeGroup';

        constructor() {
            super();

            this.seal(ControlGroup);
        }

        static transclude = true;
        static template =
            `<div name="{{$ctrl.name}}" id="{{$ctrl.id}}" ng-show="{{$ctrl.inShow}}"><ng-transclude></ng-transclude></div>`;

    }

}
/// <reference path="../../../typings/index.d.ts" />

namespace Safe.System {
    /** Represents an object that might contain information about an error */
    export interface IPotentialError {
        message?: string;
        stack?: string;
        errorDescription?: string;
        statusText?: string;
    }

    interface IErrorExt extends Error {

    }

    export interface IHttpConfig {
        url: string;
    }

    export interface IHttpRejection {
        status: HttpStatusCode;
        statusText: string;
        config: IHttpConfig
    }

    export interface IHttpServerValidationRejection extends IHttpRejection {
        data: any;
    }

    // TODO: Sooo much more
    export class Debug {
        static stop(msg?: string) {
            msg && console.warn(msg);
            Debug.stopIf();
        }

        static stopIf(predicate: () => boolean = () => true) {
            if (predicate()) {
                debugger;
            }
        }

        static countdown(counter: Counter = new Counter()) {
            if (counter.count) {
                --counter.count;
                Debug.stop();
            }
        }
    }

    export class Counter {
        constructor(public count: number = 1) {

        }
    }

    export class Assert {
        static isDefined(v: any): void {
            if (_.isUndefined(v)) {
                throw new AssertionException('Assert.isDefined');
            }
        }

        static isNotNull(v: any): void {
            if (v === null) {
                throw new AssertionException('Assert.isNotNull');
            }
        }

        static isNull(v: any): void {
            if (v !== null) {
                throw new AssertionException('Assert.isNull');
            }
        }

        static hasValue(v: any): void {
            Assert.isDefined(v);
            Assert.isNotNull(v);
        }

        static isTrue(v: boolean): void {
            if (!v) {
                throw new AssertionException('Assert.isTrue');
            }
        }

        static isFalse(v: boolean): void {
            if (v) {
                throw new AssertionException('Assert.isFalse');
            }
        }

        static isNotEmpty(v: any[] | JQuery) {
            Assert.isDefined(v);
            if (v.length === 0) {
                throw new AssertionException('Assert.isNotEmpty');
            }
        }
    }

    type PotentialError = IPotentialError | Error;

    export class Exception implements IErrorExt {
        static stopOnThrow = false;

        constructor(message: string | PotentialError = '') {
            if (Exception.stopOnThrow) {
                debugger;
            }

            if (message instanceof Error) {
                this.error = message;
            }
            else {
                this.error = new Error(_.isString(message) ? message : message.message || message.statusText || message.errorDescription || 'Unknown Exception');
                this.cause = message;
            }

            this.error.name = this.constructor.name;
            this._stackFrames = _.map(this.error.stack.split(/\n/), (padded: string) => padded.trim());
        }

        public toJSON(): any {
            return _.omit(this, ['_cause', 'stack', '_stackFrames', 'viewer']);
        }

        //region -- additional exception cause information
        private _cause: any = null;
        public get cause(): any {
            return this._cause;
        }

        public set cause(newVal: any) {
            // don't allow resetting the cause. This is a set-once property.
            this._cause = this._cause || newVal;
        }

        //endregion

        private _viewer: UI.IExceptionViewer;
        public get viewer(): UI.IExceptionViewer {
            if (!this._viewer) {
                this._viewer = UI.ExceptionViewer;
            }
            return this._viewer;
        }

        public set viewer(newVal: UI.IExceptionViewer) {
            this._viewer = newVal;
        }

        public get name(): string {
            return this.error.name;
        }

        public get message() {
            return this.error.message;
        }

        public get stack() {
            return this._stackFrames.join('\n');
        }

        private _stackFrames: string[];
        public get stackFrames(): string[] {
            return this._stackFrames;
        }

        private error: Error;
    }

    /** Type representing system exceptions (such as Http errors) */
    export class SystemException extends Exception {
        constructor(message?: string | PotentialError) {
            super(message);
        }
    }

    /** Type representing Http exceptions */
    export class HttpException extends SystemException {
        constructor(rejection: IHttpRejection) {
            super(rejection);
        }

        // addl properties
        public get statusCode(): HttpStatusCode {
            let rejection = this.cause as IHttpRejection;
            return rejection.status;
        }

        public get badUrl(): string {
            let rejection = this.cause as IHttpRejection;
            return rejection.config && rejection.config.url;
        }
    }

    export class ServerUnavailableException extends HttpException {
        constructor(rejection: IHttpRejection) {
            super(rejection);
            this.viewer = UI.ServerUnavailableViewer;
        }
    }

    export class ServerValidationException extends HttpException {
        constructor(rejection: IHttpServerValidationRejection) {
            super(rejection);
            this.viewer = UI.ServerValidationExceptionViewer;
            if (rejection && rejection.data) {
                this.addToErrorList(rejection);
            }
        }

        public errors: any = [];

        private addToErrorList(rejection: IHttpServerValidationRejection) {
            Assert.hasValue(rejection);
            if (rejection && rejection.data) {
                for (let key in rejection.data) {
                    var errorMessage: StringMap = {};
                    errorMessage['message'] = rejection.data[key][0];
                    errorMessage[key] = rejection.data[key][0];
                    this.errors.push(errorMessage);
                }
            }
        }
    }

    /** Type representing application exceptions (such as logical errors) */
    export class ApplicationException extends Exception {
        constructor(message?: string | PotentialError) {
            super(message);
        }
    }

    export class FoundationException extends Exception {
        constructor(message?: string | PotentialError) {
            super(message);
        }
    }

    /** Type representing exceptions from the Angular framework */
    export class AngularException extends Exception {
        constructor(message?: string | PotentialError) {
            super(message);
        }
    }

    /** Type representing application validation exceptions */
    export class ValidationException extends ApplicationException {
        constructor(message?: string | PotentialError) {
            super(message);
        }
    }

    /** Type representing exception from running code which is not implemented */
    export class NotImplementedException extends ApplicationException {
        constructor(message?: string | PotentialError) {
            super(message);
        }

    }

    /** Type representing exception from a failed assertion */
    export class AssertionException extends ApplicationException {
        constructor(message?: string | PotentialError) {
            super(message);
        }

    }


    /* Debugging tool - developer use only */
    export class Break {
        static stop() {
            debugger;
        }
    }
}

namespace Safe {
    // TODO: Refactor regions into files....

    //region binding support

    //region old-style Angular support
    export type NgBindingType = '@' | '<' | '=' | '&';

    /** Represents an object containing the bindings for a component */
    export type INgBinding = StringMap;
    //endregion

    export enum VarType {
        Unknown,
        Null,
        Any,
        Function,
        String,
        Date,
        Time,
        DateTime,
        Percentage,
        Currency,
        Boolean,
        Number,
        WholeNumber,
        RationalNumber,
        Object,
        CompoundObject,
        SimpleObject,
        Array,
        Bag,
        Table,
        Component,
    }
    export type BoundType = VarType | object;   // object is for enum support, but this can be extended

    /** our extended binding type */

    export abstract class Binding {
        constructor(public name: string,
                    public type: BoundType,
                    public defaultValue?: any,
                    public required?: boolean) {
        }

        public static isBinding(obj: any): obj is Binding {
            return _.isObject(obj) && !_.isUndefined(obj.bindingMarker) && obj.bindingMarker instanceof Binding;
        }

        private __S_smart = false;
        public useSmartVar(): Binding {
            this.__S_smart = true;
            return this;
        }

        public get smartVarEnabled(): boolean {
            return this.__S_smart;
        }

        public toString() {
            return `Binding ${this.constructor.name}('${this.name})')`;
        }

        static ToNgBinding(binding: Binding | Bindings): INgBinding {
            let bindings = _.isArray(binding) ? binding : [binding];
            let ret: INgBinding = {};

            _.each(bindings, binding => {
                ret[binding.name] = (<any>binding.constructor)['ngSymbol'] + (binding.required ? '' : '?');
            });

            return ret;
        }

        // this neat thing makes sure I can check for a Binding type if/when it's passed - see SmartVar ctor
        private bindingMarker: Binding = this;
    }

    export class NullBinding extends Binding {
        constructor(public name: string = 'val',
                    public type: BoundType = VarType.Unknown,
                    public defaultValue?: any,
                    public required: boolean = false) {
            super(name, type, defaultValue, required);
        }

        public useSmartVar(): never {
            throw new System.FoundationException('Cannot use smart variable for NullBinding');
        }

        static ngSymbol = '!!BAADF00D!!';
    }

    export class StaticBinding extends Binding {
        constructor(public name: string,
                    public type: BoundType = VarType.String,
                    public defaultValue?: any,
                    public required = false) {
            super(name, type, defaultValue, required);

            if (name.indexOf('in') === 0 ||
                name.indexOf('io') === 0 ||
                name.indexOf('out') === 0 ||
                name.indexOf('on') === 0) {
                throw new System.FoundationException(`Static binding name must NOT start with 'in', 'io', 'out', or 'on'. ${this.toString()}"`);
            }
        }

        static ngSymbol = '@';
    }

    export class InputBinding extends Binding {
        constructor(public name: string,
                    public type: BoundType = VarType.Unknown,
                    public defaultValue?: any,
                    public required = false) {
            super(name, type, defaultValue, required);

            if (name.indexOf('in') != 0) {
                throw new System.FoundationException(`Input binding name must start with 'in'. ${this.toString()}"`);
            }
        }

        static ngSymbol = '<';
    }

    export class TwoWayBinding extends Binding {
        constructor(public name: string,
                    public type: BoundType = VarType.Unknown,
                    public defaultValue?: any,
                    public required = false) {
            super(name, type, defaultValue, required);

            if (!(name.indexOf('out') !== 0 || name.indexOf('io') !== 0)) {
                throw new System.FoundationException(`Two-way binding name must start with 'out' or 'io'. ${this.toString()}"`);
            }
        }

        static ngSymbol = '=';
    }

    export class CallbackBinding extends Binding {
        constructor(public name: string,
                    public type: BoundType = VarType.Function,
                    public defaultValue: any = null,
                    public required = false) {
            super(name, type, defaultValue, required);

            if (name.indexOf('on') != 0) {
                throw new System.FoundationException(`Callback binding name must start with 'on'. ${this.toString()}"`);
            }

            if (name !== 'onEvent') {
                console.warn('====== SUSPICIOUS CALLBACK BINDING USE ======');
                console.warn(`====== ${name} ======`);
                console.warn('>> ARE YOU SURE TWO-WAY BINDING ISN\'T BETTER??');
                console.warn('>> ARE YOU SURE USING ONEVENT WON\'T DO??');
                console.warn('====== SUSPICIOUS CALLBACK BINDING USE ======');
            }
        }

        static ngSymbol = '&';
    }

    export type Bindings = Binding[];

    //endregion

}

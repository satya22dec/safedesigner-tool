/**
 * Created by mtelb on 4/27/17.
 */

namespace Safe {
    export namespace Interfaces {
        export interface IUser {
            Id: number;
            UserName: string;
            FirstName: string;
            LastName: string;
            Email: string;
            PhoneNumber: string;
        }
    }

}
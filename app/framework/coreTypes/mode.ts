
namespace Safe.System {
    export class Mode {
        constructor(public appName: string) {

        }

        public reportExceptions = true;
    }

    export class DebugMode extends Mode {
        constructor(appName: string) {
            super(appName);
        }
    }

    export class ProductionMode extends Mode {
        constructor(appName: string) {
            super(appName);
        }
    }
}
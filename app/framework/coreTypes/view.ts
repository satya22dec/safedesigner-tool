namespace Safe {

    export interface IViewClass extends Function {
        new (...args: any[]): View;
        $inject: string[];
        ngName: string;
        templateUrl: string;
    }
    export type ViewClasses = IViewClass[];


    export abstract class View {
        // TODO: ...
        static ngName = '&%^^&BAADF00D';
        static templateUrl = '';
    }
}

var Safe;
(function (Safe) {
    /** Represents a Safe component */
    var Component = (function () {
        //endregion
        /** Initializes a new instance of this class */
        function Component() {
            // special...
            this.ioThis = this;
            this.createSmartVars();
            if (this.idProp.isUndefined) {
                var ctor = this.constructor;
                var id = Safe.randomId(ctor.ngName);
                this.idProp.setIf(id);
            }
        }
        Component.GetAllBindings = function () {
            return Component.allBindings(this);
        };
        //region lifecycle management
        /**
         * Called on each controller after all the controllers on an element have been constructed and had their bindings
         * initialized (and before the pre & post linking functions for the directives on this element). This is a good
         * place to put initialization code for your controller.
         */
        Component.prototype.$onInit = function () {
        };
        /**
         * Called whenever one-way bindings are updated. The changesObj is a hash whose keys are the names of the bound
         * properties that have changed, and the values are an {@link IChangesObject} object  of the form
         * { currentValue, previousValue, isFirstChange() }. Use this hook to trigger updates within a component such as
         * cloning the bound value to prevent accidental mutation of the outer value.
         */
        Component.prototype.$onChanges = function (changesObj) {
            changesObj;
        };
        /**
         * Called on a controller when its containing scope is destroyed. Use this hook for releasing external resources,
         * watches and event handlers.
         */
        Component.prototype.$onDestroy = function () {
            //this.parent.removeChild(this);
        };
        /**
         * Called after this controller's element and its children have been linked. Similar to the post-link function this
         * hook can be used to set up DOM event handlers and do direct DOM manipulation. Note that child elements that contain
         * templateUrl directives will not have been compiled and linked since they are waiting for their template to load
         * asynchronously and their own compilation and linking has been suspended until that occurs. This hook can be considered
         * analogous to the ngAfterViewInit and ngAfterContentInit hooks in Angular 2. Since the compilation process is rather
         * different in Angular 1 there is no direct mapping and care should be taken when upgrading.
         */
        Component.prototype.$postLink = function () {
            //this.discoverChildren();
        };
        Object.defineProperty(Component.prototype, "$compile", {
            get: function () {
                if (this._$compile) {
                    this._$compile = angular.injector().get('$compile');
                }
                return this._$compile;
            },
            enumerable: true,
            configurable: true
        });
        Component.prototype.seal = function (ctor) {
            var myName = this.constructor.name;
            if (!(this instanceof ctor)) {
                throw new Safe.System.FoundationException("Attempt to derive from a sealed type. Base: " + ctor.name + "; Derived: " + myName);
            }
        };
        Object.defineProperty(Component.prototype, "allBindings", {
            get: function () {
                if (_.isUndefined(this._allBindings)) {
                    this._allBindings = Component.allBindings(this);
                }
                return this._allBindings;
            },
            enumerable: true,
            configurable: true
        });
        //endregion
        Component.prototype.createSmartVars = function () {
            var _this = this;
            var anyMe = this;
            _.each(this.allBindings, function (binding) {
                if (binding.smartVarEnabled) {
                    Safe.SmartVar.BindTo(binding, _this);
                }
                else {
                    if (_.isUndefined(anyMe[binding.name])) {
                        anyMe[binding.name] = binding.defaultValue;
                    }
                    if (binding.required && _.isUndefined(anyMe[binding.name])) {
                        throw new Safe.System.FoundationException("Missing required binding " + binding.toString());
                    }
                }
            }, this);
        };
        //endregion
        // region child management
        // TODO: Consider moving this to a mixin type
        Component.prototype.addChild = function (child) {
            if (!_.contains(this.children, child)) {
                this.children.push(child);
                child._parent = this;
            }
            return this;
        };
        Component.prototype.removeChild = function (child) {
            child._parent = null;
            this._children = _.without(this.children, child);
            return this;
        };
        Component.prototype.getChild = function (id) {
            return _.find(this.children, function (child) { return child.idProp.eq(id); });
        };
        /** Default predicate selects all children */
        Component.prototype.getChildren = function (selector) {
            if (selector === void 0) { selector = function () { return true; }; }
            return _.select(this.children, selector);
        };
        Component.prototype.getChildrenOfType = function (type) {
            return this.getChildren(function (c) { return c instanceof type; });
        };
        Object.defineProperty(Component.prototype, "children", {
            get: function () {
                if (_.isUndefined(this._children)) {
                    this.discoverChildren();
                }
                return this._children;
            },
            enumerable: true,
            configurable: true
        });
        Component.prototype.discoverChildren = function () {
            var _this = this;
            this._children = [];
            var jChildren = $(document.getElementById(this.idProp.value)).filter(function (idx, el) { return el.tagName.toLowerCase().indexOf('safe-') === 0; });
            jChildren.each(function (idx, el) {
                debugger;
                var jElement = $(el);
                var control = jElement.controller(el.tagName.dedasherize());
                control && _this.addChild(control);
            });
        };
        Object.defineProperty(Component.prototype, "parent", {
            get: function () {
                return this._parent;
            },
            enumerable: true,
            configurable: true
        });
        //endregion
        //region internal use
        Component.allBindings = function (ctor) {
            ctor = _.isFunction(ctor) ? ctor : ctor.constructor;
            if (ctor.bindings) {
                return [];
            }
            return (function r(cc, ret) {
                if (ret === void 0) { ret = []; }
                ret = _.uniq(ret.concat(cc.exBindings));
                return (cc.prototype instanceof Component) ? r(Object.getPrototypeOf(cc), ret) : ret;
            })(ctor);
        };
        return Component;
    }());
    //region statics for IComponentClass
    Component.$inject = [];
    Component.ngName = '!!-BAADF00D,1';
    Component.template = '<H1>BAD TEMPLATE</H1>';
    Component.transclude = false;
    Component.require = {};
    //region bindings
    Component.exBindings = [
        new Safe.StaticBinding('id').useSmartVar(),
        new Safe.TwoWayBinding('ioValue', Safe.VarType.Any).useSmartVar(),
        // TODO: special. rarely (I hope) used binding to expose the controller more easily...
        new Safe.TwoWayBinding('ioThis', Safe.VarType.Component)
    ];
    Safe.Component = Component;
})(Safe || (Safe = {}));
//# sourceMappingURL=ccc_component.js.map
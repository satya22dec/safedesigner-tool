namespace Safe {
    //region derived, specialty classes

    export abstract class Control extends Component {
        constructor() {
            super();

            this.inShowProp.setIf(true);
        }

        public $onInit() {
            super.$onInit();
        }

        public $onDestroy() {
            super.$onDestroy();
        }

        //region bindings
        static exBindings: Binding[] = [
            new StaticBinding('className', VarType.Bag).useSmartVar(),
            new InputBinding('inStyle', VarType.Object).useSmartVar(),
            new InputBinding('inShow', VarType.Boolean, true).useSmartVar(),
        ];

        // private className: string[];
        public readonly classNameProp: SmartStringBag;
        // private inStyle: any;
        public readonly inStyleProp: SmartAny;
        // private inShow: boolean;
        public readonly inShowProp: SmartBool;

        //endregion

    }
    export type Controls = Control[];

    export abstract class InteractiveControl extends Control {
        constructor() {
            super();

            this.inDisabledProp.setIf(false);
        }

        //region bindings

        static exBindings: Binding[] = [
            new InputBinding('inDisabled', VarType.Boolean, false).useSmartVar(),
            new CallbackBinding('onEvent').useSmartVar(),
        ];

        // private inDisabled: boolean;
        public readonly inDisabledProp: SmartBool;
        // private onEvent: Function;
        public readonly onEventProp: SmartCall<SafeEvent, void>;

        //endregion

        protected fireEvent(type: SafeEventType, associatedData?: any) {
            this.onEventProp.invoke(new SafeEvent(this, type, associatedData));
        }
    }

    export abstract class NamedControl extends InteractiveControl {
        constructor() {
            super();

            this.nameProp.setIf(this.constructor.name);
        }

        static exBindings: Binding[] = [
            new StaticBinding('name').useSmartVar(),
        ];

        //private name: string;
        public readonly nameProp: SmartString;
    }

    export abstract class ComplexControl extends NamedControl {
        constructor() {
            super();
        }

        static exBindings: Binding[] = [
            new InputBinding('inPut', VarType.Any).useSmartVar(),
        ];

        public $postLink() {
            super.$postLink();

            // NOTE: Assert that a complex control gets input (NULL is fine)
            this.inPutProp.assert();
        }

        //private inPut: any;
        public readonly inPutProp: SmartAny;
    }

    export abstract class FormControl extends NamedControl {
        static defaultClasses = ['form-control'];
        constructor() {
            super();

            this.classNameProp.init(FormControl.defaultClasses);
        }
    }

    //endregion
}
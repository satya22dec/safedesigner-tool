namespace Safe {
    let __gNullBinding: Binding = new NullBinding();

    //region SmartVar support

    export enum SmartFormatFlags {
        None = 0x0,

        Short = 0x1,
        Medium = 0x2,
        Long = 0x4,

        Whole = 0x100,
        DateOnly = 0x200,
        TimeOnly = 0x400,

        Default = Medium & Whole,
    }

    //region base SmartVar

    export type LikeVar<T> = T|SmartVar<T>;

    export class SmartVar<T> {
        static BindTo(binding: Binding, obj: AnyObj): SmartVar<any> {
            // TODO: Throw exception if the provided type mismatches the data value type.
            if (!(obj && binding)) {
                throw new System.FoundationException(`Cannot create a factory SmartVar without object binding information`);
            }

            let currentVal = obj[binding.name];
            if (isSmartVar(currentVal)) {
                return currentVal;
            }

            // create a new property on bound object iff it's not there already
            if (_.isUndefined(currentVal)) {
                Object.defineProperty(obj, binding.name, {
                    value: binding.defaultValue,
                    configurable: true,
                    writable: true,
                    enumerable: true
                });
                currentVal = binding.defaultValue;
            }

            let smartVar: SmartVar<any>;
            if (binding instanceof CallbackBinding) {
                if (binding.defaultValue) {
                    throw new System.FoundationException(`Cannot have default value for callback binding. ${binding.toString()}`);
                }
                smartVar = new SmartCall(binding, obj);
            }
            else if (_.isObject(binding.type)) {
                smartVar = new SmartEnum(binding.type, binding, obj);
            }
            else {
                switch (binding.type) {
                    case VarType.Date:
                    case VarType.Time:
                    case VarType.DateTime:
                        smartVar = new SmartDate(binding, obj);
                        break;

                    case VarType.Number:
                    case VarType.Currency:
                    case VarType.RationalNumber:
                    case VarType.WholeNumber:
                        smartVar = new SmartNumber(binding, obj);
                        break;

                    case VarType.Boolean:
                        smartVar = new SmartBool(binding, obj);
                        break;

                    case VarType.Object:
                    case VarType.SimpleObject:
                    case VarType.CompoundObject:
                        smartVar = new SmartObj(binding, obj);
                        break;

                    case VarType.String:
                        smartVar = new SmartString(binding, obj);
                        break;

                    case VarType.Any:
                        smartVar = new SmartVar(binding, obj);
                        break;

                    case VarType.Array:
                    case VarType.Table:    // array of objects
                    case VarType.Bag:
                        smartVar = new SmartArray(binding, obj);
                        break;

                    case VarType.Component:
                        smartVar = new SmartRef(binding, obj);
                        break;

                    default:
                        // discover
                        if (_.isString(currentVal))
                            smartVar = new SmartString(binding, obj);
                        else if (_.isNumber(currentVal))
                            smartVar = new SmartNumber(binding, obj);
                        else if (_.isDate(currentVal))
                            smartVar = new SmartDate(binding, obj);
                        else if (_.isBoolean(currentVal))
                            smartVar = new SmartBool(binding, obj);
                        else if (_.isArray(currentVal))
                            smartVar = new SmartArray(binding, obj);
                        else if (_.isObject(currentVal))
                            smartVar = new SmartObj(binding, obj);

                        break;
                }
            }

            if (!smartVar) {
                console.warn(`Unable to find appropriate SmartVar for ${binding.toString()}. Using SmartAny`);
                smartVar = new SmartVar<any>(binding, obj);
            }

            // and, finally, create the proxy object on the bound object
            obj[`${binding.name}Prop`] = smartVar;

            return smartVar;
        }

        // TODO: Kinda ugly to accept any as the param - but this type's raison d'etre is to wrap an existing property, so...
        constructor(init?: any, boundObj?: AnyObj) {
            this.boundTo = boundObj || this;
            this.binding = __gNullBinding;

            if (Binding.isBinding(init)) {
                this.binding = init;    // being bound to property
            }
            else {
                this.set(init);        // using stand alone, using initial value
            }

            if (this.binding.required && this.isUndefined) {
                throw new System.FoundationException(`Missing required binding. ${this.binding.toString()}`)
            }

            this.initialValue = this.value;
        }

        protected binding: Binding;
        private boundTo: AnyObj;
        private _initialValue: T;

        public get value(): T {
            return this.preGet(this.boundTo[this.binding.name]);
        }

        protected preGet(currentVal: any): T {
            return currentVal;
        }
        protected preSet(newVal: any): T {
            return newVal;
        }

        public toString(fmt?: SmartFormatFlags): string {
            return this.hasValue ? this.value.toString() : '';
        }

        public get initialValue(): T {
            return this._initialValue;
        }
        public set initialValue(newVal: T) {
            this._initialValue = newVal;
        }

        // private for HTML/markup use only
        private get ngValue() {
            return this.value;
        }
        private set ngValue(newVal) {
            this.set(newVal);
        }

        // region helpers

        public get forMarkup() {
            return this.toString();
        }

        public eq(other: LikeVar<T>): boolean {
            return this.value === smartVal(other);
        }
        public neq(other: LikeVar<T>): boolean {
            return !this.eq(other);
        }

        public set(newVal?: LikeVar<T>): SmartVar<T> {
            if (_.isUndefined(newVal)) {
                if (this.binding.required) {
                    throw new System.FoundationException(`Attempt to clear required bound value. ${this.binding.toString()}`)
                }
                this.boundTo[this.binding.name] = newVal;
            }
            else {
                let preppedValue = isSmartVar(newVal) ? this.preSet(newVal.value) : this.preSet(newVal);
                this.boundTo[this.binding.name] = preppedValue;
            }
            return this;
        }

        /** Default is set if undefined */
        public setIf(newVal?: LikeVar<T>, predicate: Predicate<SmartVar<T>> = sv => sv.isUndefined): SmartVar<T> {
            if (predicate(this)) {
                this.set(newVal);
            }
            return this;
        }

        /* Default is throw if undefined */
        public assert(truth?: Predicate<SmartVar<T>>): SmartVar<T> {
            if (_.isUndefined(truth)) {
                if (this.isNullOrUndefined) {
                    throw new System.AssertionException();
                }
            }
            else if (_.isFunction(truth) && !truth(this)) {
                throw new System.AssertionException();
            }
            else if (isSmartVar(truth) && this.value !== truth.value ) {
                throw new System.AssertionException();
            }
            else if (this.value !== truth.valueOf()) {
                throw new System.AssertionException();
            }
            return this;
        }

        public valueOf(): any {
            return this.value;
        }

        public get isDirty(): boolean {
            return this.value !== this.initialValue;
        }

        public clean(): SmartVar<T> {
            this.initialValue = this.value;
            return this;
        }

        public reset(): SmartVar<T> {
            this.set(this.initialValue);
            return this;
        }

        public clear(): SmartVar<T> {
            this.set();
            return this;
        }

        public get isNull(): boolean {
            return this.value === null;
        }

        public get isUndefined(): boolean {
            return _.isUndefined(this.value);
        }

        public get isNullOrUndefined(): boolean {
            return this.isUndefined || this.isNull;
        }

        public get isEmpty(): boolean {
            return _.isEmpty(this.value);
        }

        public get hasValue(): boolean {
            return !this.isNullOrUndefined;
        }

        public get watchExpression() {
            return `$ctrl.${this.binding.name}`;
        }

        public watch(): SmartVar<T> {
            if (this.boundTo === this || !(this.boundTo instanceof Component)) {
                throw new System.FoundationException('Cannot watch SmartVar not bound to a component');
            }

            let boundObj: AnyObj = this.boundTo;
            let scope: IExtendedScope = boundObj['$scope'];

            if (!scope) {
                throw new System.FoundationException('Cannot watch SmartVar outside of component scope');
            }

            let exp = this.watchExpression;
            if (!_.any(scope.$$watchers, (w) => _.isString(w.exp) && w.exp.toUpperCase() === exp.toUpperCase())) {
                scope.$watch(exp);
            }

            return this;
        }

        /** Combines watch and set (if undefined) */
        public poke(val: LikeVar<T>): SmartVar<T> {
            return this.isUndefined ? this.watch().set(val) : this;
        }

        //endregion
    }

    //endregion

    // NOTE: IMPORTANT. Unless I want to spend a lot of time ordering files, just leave these here
    //region SmartVar type variants

    export type SmartAny = SmartVar<any>;

    export class SmartCall<TParam, TReturn> extends SmartVar<(p:TParam)=>TReturn> {
        constructor(init?: any, boundObj?: object) {
            super(init, boundObj);
        }

        public invoke(arg?: TParam): TReturn {
            if (this.hasValue) {
                // create the funk-o-matic params object that ng luvs
                let params: AnyObj = {};
                params[this.binding.name.toLowerCase().slice(2)] = arg;
                this._retVal = this.value(<TParam>params);
                return this.retVal;
            }

            return undefined;
        }

        protected preSet(newVal: any): never {
            throw new System.FoundationException(`Attempt to set ${newVal} on callback bound value. ${this.binding.toString()}`);
        }

        private _retVal: TReturn;
        public get retVal(): TReturn {
            return this._retVal;
        }
    }


    export class SmartBool extends SmartVar<boolean> implements Boolean{
        constructor(init?: any, boundObj?: object) {
            super(init, boundObj);
        }

        protected preSet(newVal: any): boolean {
            return !!newVal;
        }

        public toggle(): SmartBool {
            this.set(!this.value);
            return this;
        }

        public get on(): boolean {
            return this.value;
        }

        public get off(): boolean {
            return this.value;
        }

        public get yes(): boolean {
            return this.on;
        }

        public get no(): boolean {
            return this.off;
        }
    }


    export class SmartDate extends SmartVar<moment.Moment> {
        constructor(init?: any, boundObj?: object) {
            super(init, boundObj);

            this.setIf(moment());
        }

        protected preGet(currentVal: moment.Moment) {
            return currentVal.clone();
        }

        protected preSet(newVal: any): moment.Moment {
            let m: moment.Moment = null;

            if (moment.isMoment(newVal)) {
                m = newVal.clone();
            }
            else if (_.isDate(newVal)) {
                m = moment(newVal);
            }
            else if (_.isString(newVal)) {
                // TODO: Warn?

                m = moment(newVal);
                if (!m.isValid()) {
                    m = null;
                }
            }

            if (m) {
                if (this.binding.type === VarType.Date) {
                    m.startOf('d');    // mutates the object
                }
                return m;
            }

            throw new System.FoundationException(`Attempt to set invalid date on bound value. ${this.binding.toString()}`);
        }

        public toString(fmt?: SmartFormatFlags): string {
            return this.format(fmt);
        }

        // TODO: Formatting ... ugh
        public format(fmt: SmartFormatFlags = SmartFormatFlags.Default): string {
            // TODO: Add more ; use formats
            let stringFormat = this.binding.type === VarType.Date ? 'dddd' : 'LLL';
            if (_.isString(fmt)) {
                stringFormat = fmt;
            }
            return moment(this.value).format(stringFormat);
        }

        public valueOf(): any {
            return this.value.valueOf();
        }

        public addDay(num: number = 1): SmartDate {
            this.set(this.value.add(num, 'd'));
            return this;
        }

        public addHour(num: number = 1): SmartDate {
            this.set(this.value.add(num, 'h'));
            return this;
        }

        public addMinute(num: number = 1): SmartDate {
            this.set(this.value.add(num, 'm'));
            return this;
        }
    }


    export class SmartNumber extends SmartVar<number> {
        constructor(init?: any, boundObj?: object) {
            super(init, boundObj);
        }

        static GetRandom(min: number = 0, max: number = 1000000000): SmartVar<number> {
            return new SmartNumber(_.random(min, max));
        }

        protected preSet(newVal: any): number {
            if (_.isNumber(newVal))
                return newVal;

            if (_.isString(newVal)) {
                let convert = parseFloat(newVal);
                // TODO: This could set the value to NaN, which is prolly what we want...
                //if (_.isNumber(convert)) {
                return convert;
                //}
            }

            throw new System.FoundationException(`Attempt to set invalid number on bound value. ${this.binding.toString()}`);
        }
    }


    export class SmartEnum<T> extends SmartVar<T> {
        constructor(private enumType: any, init?: any, boundObj?: object) {
            super(init, boundObj);

            // run the value thru assignment to convert to enum
            this.set(this.value);
        }

        protected preSet(newVal: any): T {
            let e: any = undefined;

            if (_.isNumber(newVal)) {
                if (!_.isUndefined(this.enumType[newVal])) {
                    e = newVal;             // return number, not looked up string
                }
            }
            else if (_.isString(newVal)) {
                e = this.enumType[newVal];  // string lookup, may be undefined
            }

            if (!_.isUndefined(e)) {
                return e;
            }

            throw new System.FoundationException(`Attempt to set illegal enum value. ${this.binding.toString()}`);
        }

        public toString() {
            return this.enumType[this.value];
        }
    }


    export class SmartObj<T extends object> extends SmartVar<T> {
        constructor(init?: any, boundObj?: object) {
            super(init, boundObj);
        }

        protected preSet(newVal: any): T {
            if (_.isObject(newVal)) {
                return newVal;
            }
            if (_.isString(newVal)) {
                return JSON.parse(newVal);
            }

            throw new System.FoundationException(`Attempt to set invalid object on bound value. ${this.binding.toString()}`);
        }

        public getAt(path: string): SmartAny {
            path;
            // let working = this.value;
            // let smartVar: SmartAny = null;
            // let parts = path.split('.');
            //
            // _.each(parts, part => {
            //     working = working[part];
            //     smartVar = (working instanceof SmartVar) ? working : SmartVar.BindTo(new NullBinding(part), working);
            //     // TODO: Something something something....
            // });
            throw new System.NotImplementedException();
            //return smartVar;
        }
    }

    export class SmartString extends SmartVar<string> {
        constructor(init?: any, boundObj?: object) {
            super(init, boundObj);
        }

        protected preSet(newVal: any) {
            let s: string = undefined;

            if (!(_.isObject(newVal) || _.isArray(newVal))) {
                s = newVal.toString();
            }

            if (s && this.autoValidate) {
                this.assertMatch();
            }

            if (!_.isUndefined(s)) {
                return s;
            }

            throw new System.FoundationException(`Attempt to set invalid string on bound value. ${this.binding.toString()}`);
        }

        //region helpers

        public get seemsLikeId(): boolean {
            // TODO: Use RegEx ??? Right now, cheat...
            if (this.hasValue) {
                let v = this.value;
                // reasonable length, containing dot, and no spaces
                if (v.length > 8 && v.length < 128 && /\./.test(v)) {
                    return !/\s/.test(v);
                }
            }
            return false;
        }

        public get isBlank(): boolean {
            return this.isNullOrUndefined || /\S/.test(this.value);
        }

        public contains(str: string): boolean {
            return this.hasValue && this.value.indexOf(str) !== -1;
        }

        public autoValidate: boolean = false;
        private pattern = /.*/;

        public match(regex: RegExp = this.pattern): boolean {
            return regex.test(this.value);
        }

        public assertMatch(regex: RegExp = this.pattern): SmartString {
            if (!this.match(regex)) {
                throw new System.AssertionException();
            }
            return this;
        }

        //endregion
    }

    export class SmartArray<T> extends SmartVar<T[]> {
        constructor(init?: any, boundObj?: object) {
            super(init, boundObj);
        }

        protected preSet(newVal: any) {
            let a: any = null;
            if (_.isArray(newVal)) {
                a = ([].concat(newVal));
            }

            if (this.binding.type === VarType.Bag && _.isString(newVal)) {
                a = newVal.split(this.separator);
            }

            if (a) {
                if (this.binding.type === VarType.Bag) {
                    a = _.uniq(a);
                }
                return a;
            }

            throw new System.FoundationException(`Attempt to set invalid array type. ${this.binding.toString()}`)
        }

        public separator = ' ';

        public toString(): string {
            if (this.isNullOrUndefined) return '';
            return this.binding.type === VarType.Bag ? this.value.join(this.separator) : this.valueOf().toString();
        }

        public valueOf() {
            return this.value;
        }

        public add(item: T|T[]): SmartArray<T> {
            this.setIf([]);
            let items = _.isArray(item) ? item : [item];

            _.each(items, item => this.value.push(item));
            if (this.binding.type === VarType.Bag) {
                this.set(_.uniq(this.value));
            }
            return this;
        }

        public addIf(items: T|T[], predicate: Predicate<SmartArray<T>> = (sa) => sa.isUndefined): SmartArray<T> {
            return predicate(this) ? this.add(items) : this;
        }

        public remove(item: T | T[]): SmartArray<T> {
            if (this.hasValue) {
                let items: T[] = _.isArray(item) ? item : [item];
                this.set(_.difference(this.value, items));
            }
            return this;
        }

        public removeAll(): SmartArray<T> {
            // don't set an undefined value to empty
            this.setIf([], ()=>this.hasValue);
            return this;
        }

        public init(item: T|T[]): SmartArray<T> {
            return this.hasValue ? this : this.removeAll().add(item);
        }

        public contains(item: T | T[], all: boolean = true): boolean {
            if (this.hasValue) {
                let items: T[] = _.isArray(item) ? item : [item];
                let diff = _.difference(this.value, items);
                if (diff.length) {
                    return (diff.length === items.length) ? false : !all;
                }
                else {
                    // no difference
                    return true;
                }
            }
            return false;
        }
    }
    export type SmartStringBag = SmartArray<string>;
    export type SmartNumberBag = SmartArray<number>;

    export class SmartRef<T extends Component> extends SmartVar<T> {
        constructor(init?: any, boundObj?: object) {
            super(init, boundObj);
            throw new System.NotImplementedException();
        }

        // TODO: Helpers for doing component stuff
    }

    //endregion


}
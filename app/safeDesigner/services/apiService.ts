
export class ApiService {

    constructor(private $http: ng.IHttpService, private $q: ng.IQService) {
    }
    qsComponentData: any;
    getLibraryComponents(): ng.IPromise<any[]> {
        return this.$q(resolve => {
            this.$http.get('./app/qsLibrary/library.config.json').then(response => {
                return resolve(response.data);
            });
        });
    }
}


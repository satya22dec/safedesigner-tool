export class HtmlComponentService {
    /**
             Returns draggable element details as per the component type
        */
    constructor(private $http: ng.IHttpService, private $q: ng.IQService) {
    }
    getFieldDetails = function (field) {
        let inp = "";
        if (field === 'input') {
            inp = `
                <div class="form-group " draggable="true" onDragstart="angular.element(this).scope().$ctrl.drag(event)" >
                    <label  class="form-label"  >Default Label</label>
                    <input type="text"  placeholder="Placeholder"  class="form-control" name="test" ">
                    </div>`;
        } else if (field === 'dropdown') {
            inp = `
                <div class="form-group " draggable="true" onDragstart="angular.element(this).scope().$ctrl.drag(event)">
                    <label  class="form-label" >Default Dropdown</label>
                    <div class="select-wrap">
                        <select class="form-control" autofocus   >
                          <option value="" >select options</option>
                        </select>
                      </div>
                </div>`;
        } else {
            inp = `
                <div class="form-group" draggable="true" onDragstart="angular.element(this).scope().$ctrl.drag(event)">
                    <div class="form-group-checkbox">                    
                    <input type="checkbox" id="random" class="form-control" name="test" /> 
                     <label for="random" class="checkbox-inline form-label"  style="text-transform:capitalize" >Default Checkbox</label>   
                    </div>
                </div>
                `;
        }
        return inp;
        // return $interpolate(inp)({ field: field });
    }
}
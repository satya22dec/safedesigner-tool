import { ApiService } from './apiService';
import { HtmlComponentService } from './htmlComponentService';


export const serviceModule = angular.module('serviceModule', [])
    .service('apiService', ApiService)
    .service('htmlComponentService', HtmlComponentService);
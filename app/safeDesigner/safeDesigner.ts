import $ = require("jquery");
let droppableField: any;
let self: any;
class safeController implements ng.IComponentController {
    showMain: boolean = false;
    showDownload: boolean = false;
    hovering: boolean = false;
    showEditPanel: boolean = false;
    deleteEle: any;
    selectedFile: any;
    fileName: string;
    private $scope;
    private $compile;
    private $filter;
    private apiService;
    private htmlComponentService;
    private $uibModal: ng.ui.bootstrap.IModalService
    public static $inject = ['$scope', '$compile', '$filter', 'apiService', 'htmlComponentService', '$uibModal']
    constructor($scope, $compile, $filter, apiService, htmlComponentService, $uibModal) {
        "ngInject";
        this.$scope = $scope;
        this.$compile = $compile;
        this.$filter = $filter;
        this.apiService = apiService;
        this.htmlComponentService = htmlComponentService;
        this.$uibModal = $uibModal;
        self = this;
    }
    $onInit() {
        self.apiService.getLibraryComponents().then(data => self.apiService.qsComponentData = data.config)
    }
    /**
          called when all the changes needs to be reset.
        */
    resetChanges() {
        this.showMain = false;
        this.showDownload = false;
        this.selectedFile = "";
        this.hovering = false;
        document.getElementById('formToEdit').innerHTML = "";
    }
    /**
          called when the template needs to be saved.
        */
    saveChanges() {

        this.showDownload = true;
        this.showMain = false;
        this.fileName = this.selectedFile.slice(0, -5);
    }
    downloadTemplate() {
        let componentList = ["input-box-component", "label-and-four-input-component", "xyz"];

        let fileToSave1 = angular.copy(document.getElementById('formToEdit').innerHTML);
        for (let value of componentList) {
            let elements: any = document.getElementsByTagName(value);
            for (let selected of elements) {
                selected.innerHTML = "";
            }
        }
        let fileToSave = document.getElementById('formToEdit').innerHTML;
        let createFileBlob = new Blob([fileToSave], { type: 'text/html' }); //text/plain
        let blobFileURL = window.URL.createObjectURL(createFileBlob);
        let fileName = this.fileName;
        let link = angular.element('<a></a>');
        link.attr('download', fileName);
        link.attr('href', blobFileURL);
        document.body.appendChild(link[0]);

        link[0].click();
        window.URL.revokeObjectURL(blobFileURL);
        // document.getElementById('formToEdit').innerHTML = fileToSave1;
        this.resetChanges();
    }
    /**
          called when the html template is uploaded.
        */
    uploadFile(ev) {
        let targetFile: any = event.target;
        let file = targetFile.files[0];
        let reader = new FileReader();
        let _this = this;
        this.showMain = true;
        this.selectedFile = file.name;
        reader.onload = function (event: any) {
            angular.element(document.getElementById('formToEdit')).append(_this.$compile(event.target.result)(_this.$scope));
            let formElements: any = angular.element(document.getElementsByClassName('form-group'));
            _this.addDragNDrop(formElements);
        };
        reader.readAsText(file);
    }
    /*Drag and Drop features*/
    addDragNDrop(el: any) {
        let _this = this;
        angular.forEach(el, function (value) {
            angular.element(value).attr('draggable', 'true');
            angular.element(value).on('dragstart', _this.drag);
        });
        angular.element(document.getElementById('formToEdit')).on('dragover', _this.allowDrop);
        angular.element(document.getElementById('formToEdit')).on('drop', _this.drop);

        angular.element(document.getElementsByClassName('form-group')).on('mouseover', function () {
            _this.hoverField(this);
        });
        angular.element(document.getElementsByClassName('form-group')).on('mouseleave', function () {
            _this.removeHoverField(this);
        });

    };
    //Show Field Actions
    hoverField(context: any) {
        let _this = this;
        if (!_this.hovering) {
            console.log("Hover COntext:", context);
            let ele = "<field-actions hovering='$ctrl.hovering' toggle-edit-panel='$ctrl.toggleEditPanel()' delete-ele='$ctrl.deleteEle' confirm-remove='$ctrl.confirmRemove()'></field-actions>";
            let compileEle = angular.element(context).append(ele);
            _this.$compile(compileEle)(_this.$scope);
            _this.hovering = true;
        }
    };
    //Confirm remove field
    confirmRemove(ev) {
        let _this = self;
        _this.deleteEle = $(event.target).parents(':eq(5)');
        console.log("Deleting", _this.deleteEle);
        _this.$uibModal.open({
            backdrop: true,
            controller: ModalController,
            controllerAs: 'modal',
            templateUrl: 'app/safeDesigner/components/main/fieldActions/confirmModal.html',
            size: 'sm',
            resolve: {
                deleteEle: () => _this.deleteEle
            }
        });
    }
    //remove field actions
    removeHoverField(context: any) {
        let _this = this;
        _this.hovering = false;
        var myEl = angular.element(document.querySelector('field-actions'));
        myEl.remove();
    };
    //Edit Field Panel
    toggleEditPanel() {
        let _this = this;
        console.log("Panel Checked Before Toggle", _this.showEditPanel);
        _this.showEditPanel = !_this.showEditPanel;
        console.log("Panel Checked After Toggle", _this.showEditPanel);
        let editEle = $(event.target).parents(':eq(5)');
        console.log("Editing", editEle);
    }
    //Delete Field
    allowDrop(ev) {
        ev.preventDefault();
    }

    drag(ev) {
        droppableField = event.target;
    }

    drop(ev) {
        ev.stopPropagation();
        ev.preventDefault();
        let data = droppableField;
        let _this = self;
        if (data.tagName && data.tagName === "DIV" && droppableField.id === "qs-elements") {
            document.getElementById('qs-elements').style.left = ev.x + "px";
            document.getElementById('qs-elements').style.top = ev.y - 110 + "px";
        }
        else if (data.tagName === "LI" && $(event.target).closest('.form-group').length > 0) {
            let elementDetails = JSON.parse(droppableField.dataset.id);
            let dataDetails = "";
            if (elementDetails.componentType === "html-component") {
                dataDetails = _this.htmlComponentService.getFieldDetails(elementDetails.component);
            } else {
                dataDetails = elementDetails.component;
            }
            $($(event.target).closest('.form-group')[0]).before(_this.$compile(dataDetails)(_this.$scope));
        }
        else if (droppableField.id !== "qs-elements" && $(event.target).closest('.form-group').length > 0 && $(event.target).closest('.form-group')[0].innerText != $(droppableField).closest('.form-group')[0].innerText) {
            droppableField.parentNode.removeChild(droppableField);
            $($(event.target).closest('.form-group')[0]).before(data);
        }

    }
    /*Drag and Drop ends*/
}
/**
 * Modal controller
 */
class ModalController {
    delEle: any;
    constructor(private $uibModalInstance: ng.ui.bootstrap.IModalServiceInstance, deleteEle) {
        console.log("THis was passed", deleteEle);
        this.delEle = deleteEle;
    }
    ok(): void {
        console.log("passed this to delete", this.delEle);
        this.delEle.remove();
        this.$uibModalInstance.close();
    }

    cancel(): void {
        this.$uibModalInstance.dismiss();
    }
}
export const safeDesigner: ng.IComponentOptions = {
    controller: safeController,
    templateUrl: 'app/safeDesigner/safeDesigner.html'
};

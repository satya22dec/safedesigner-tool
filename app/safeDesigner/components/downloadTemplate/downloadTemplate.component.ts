export const downloadTemplate: ng.IComponentOptions = {
    bindings: {
        fileName: '=',
        downloadTemplate: '&'
    },
    templateUrl: 'app/safeDesigner/components/downloadTemplate/downloadTemplate.html'
};


class editFieldPropController implements ng.IComponentController {
     /* public static $inject = ['apiService']; */
    
    constructor() {
    } 
    $onInit() {
        
    }
     
}
export const editFieldProp: ng.IComponentOptions = {
    bindings: {
       showEditPanel: '=',
       toggleEditPanel: '&'
    },
    controller: editFieldPropController,
    templateUrl: 'app/safeDesigner/components/editFieldProp/editFieldProp.html'
};



class mainController implements ng.IComponentController {
    show: boolean;
    public static $inject = []
    constructor() {
        "ngInject";
    }
    $onInit() {
    }
    inputBoxEmployeeOne: Object = {
        name: "EMPLOYEE INPUTBOXCOMPONENT"
    }
    inputBoxEmployeeTwo: Object = {
        name: "PRODUCT INPUTBOXCOMPONENT"
    }
    sectionHeaderName: string = "Header";
}
export const main: ng.IComponentOptions = {
    bindings: {
        drag: "&"
    },
    controller: mainController,
    templateUrl: 'app/safeDesigner/components/main/main.html'
};

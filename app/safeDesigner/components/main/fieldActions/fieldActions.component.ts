
class fieldActionsController implements ng.IComponentController {
     /* public static $inject = ['apiService']; */
    constructor() {
    } 
    $onInit() {
        
    }
    
    
   /* 
    closeEdit(){
        console.log("closing edit");
        angular.element(document.getElementById("editPanel")).style.width = "0";
    } */
    
}
export const fieldActions: ng.IComponentOptions = {
    bindings: {
        hovering: '=',
       showEditPanel: '=',
       toggleEditPanel: '&',
       deleteEle: '=',
       confirmRemove: '&'
    },
    controller: fieldActionsController,
    templateUrl: 'app/safeDesigner/components/main/fieldActions/fieldActions.html'
};
import { ItabName } from './../../../interface/index';

class qsElementsController implements ng.IComponentController {
    private apiService;
    public static $inject = ['apiService'];
    componentData: any;
    constructor(apiService) {
        "ngInject";
        this.apiService = apiService;
    }
    $onInit() {
        this.componentData = this.apiService.qsComponentData;
    }
    qsTabs: Array<ItabName> = [{
        key: 'qs-component',
        title: "QS Components",
        content: "QS Components List"
    }, {
        key: 'qs-widget',
        title: "QS Widgets",
        content: "QS Widgets List"
    }, {
        key: 'html-component',
        title: "HTML Elements",
        content: "Html Elememts List"
    }];
    onTabSelected(key) {

    };
}
export const qsElements: ng.IComponentOptions = {
    bindings: {
        drag: '&'
    },
    controller: qsElementsController,
    templateUrl: 'app/safeDesigner/components/main/qsElements/qsElements.html'
};
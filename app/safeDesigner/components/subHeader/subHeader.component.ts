
class subHeaderController implements ng.IComponentController {
    isFileEmpty: boolean = true;
    fileName: string = "wwwww";
    private $uibModal;
    public static $inject = ['$uibModal']
    constructor($uibModal) {
        "ngInject";
        this.$uibModal = $uibModal;
    }
    $onInit() {
    }
    openDowmloadTemplate() {
        let _this = this;
        let modalInstance = _this.$uibModal.open({
            animation: false,
            component: "downloadTemplate",
            resolve: {
                fileName: function () {
                    return _this.fileName;
                }
            }
        });
        modalInstance.result.then((data: any) => {
            console.log("data", data);
        });
    }
}
export const subHeader: ng.IComponentOptions = {
    bindings: {
        uploadFile: '&',
        saveChanges: '&',
        resetChanges: '&',
        selectedFile: '=',
        showMain: '='
    },
    controller: subHeaderController,
    templateUrl: 'app/safeDesigner/components/subHeader/subHeader.html'
};

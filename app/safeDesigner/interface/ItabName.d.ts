export interface ItabName {
    "key": string,
    "title": string,
    "content": string
}

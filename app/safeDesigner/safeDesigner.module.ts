import { downloadTemplate } from "./components/downloadTemplate/downloadTemplate.component";
import { main } from "./components/main/main.component";
import { header } from "./components/header/header.component";
import { subHeader } from "./components/subHeader/subHeader.component";
import { qsElements } from "./components/main/qsElements/qsElements.component";
import { safeDesigner } from "./safeDesigner";
import { designerArea } from "./components/main/designerArea/designerArea.component";
import { fieldActions } from "./components/main/fieldActions/fieldActions.component";
import { editFieldProp } from "./components/editFieldProp/editFieldProp.component";

export const safeDesignerModule = angular.module('safeDesignerModule', [])
    .component('safeDesigner', safeDesigner)
    .component('main', main)
    .component('designerArea', designerArea)
    .component('header', header)
    .component('subHeader', subHeader)
    .component('fieldActions', fieldActions)
    .component('editFieldProp', editFieldProp)    
    .component('downloadTemplate', downloadTemplate)
    .component('qsElements', qsElements)
    .directive('fileUpload', ['$timeout', function ($timeout) {
        return {
            link: function (scope, element, attrs) {
                element.on('change', function (evt) {
                    var validExts = new Array(".xlsx", ".xls", ".xlsm");
                    var fileExt = evt.target.files[0].name;
                    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));

                });
                element.bind('click', function () {
                    element.val('');
                });
            }
        }
    }]);

﻿
// namespace Safe.Product {

export class LabelAndFourInput implements ng.IComponentOptions {

    static $inject = [];
    constructor() {
        // super();
    }

    static bindings = {
        sectionHeaderName: '@',
        inputBoxOne: '<',
        inputBoxTwo: '<',
        inputBoxThree: '<',
        inputBoxFour: '<',
    }

    static ngName = 'labelAndFourInputComponent';
    static template = `<div class="col-sm-4 col-xs-12">
                             <h3 class="icon-circle-info">{{$ctrl.sectionHeaderName}}</h3>
                             <fieldset ng-disabled="false"> 
                               <input-box-component label-name="{{$ctrl.inputBoxOne.name}}" ></input-box-component>
                               <input-box-component label-name="{{$ctrl.inputBoxTwo.name}}" ></input-box-component>
                               
                             </fieldset>
                           </div>`;

    protected sectionHeaderName: string;
    // protected inputBoxOne: { name: string, id: string; type: string; value: string; }
}
// }
// angular
//     .module('common.nav').component(Safe.Product.LabelAndFourInput.ngName, Safe.Product.LabelAndFourInput)
﻿import * as angular from 'angular';

// namespace Safe.Product {

export class InputBoxComponent {

    static $inject = [];
    constructor() {
        // super();
    }

    static bindings = {
        labelName: '@',
        inputType: '@',
        inputId: '@',
        inputName: '@',
        inputValue: '@'
    }
    static ngName = 'inputBoxComponent';
    static template = `<div class="form-group">
                             <label for="$ctrl.inputId" class="form-label">{{$ctrl.labelName}}</label>
                             <input class="form-control" type="$ctrl.inputType" id="$ctrl.inputId" name="$ctrl.inputName" ng-model="$ctrl.inputValue" />
                           </div>`;

    protected labelName: string;
    protected inputType: string;
    protected inputId: string;
    protected inputName: string;
    protected inputValue: string;

}
// export class InputBoxComponent implements ng.IComponentOptions {

//     public bindings: {
//         labelName: '@',
//         inputType: '@',
//         inputId: '@',
//         inputName: '@',
//         inputValue: '@'
//     }
//     public controller: Function = InputController;
//     static ngName = 'inputBoxComponent';
//     public template: string = `<div class="form-group">
//                              <label for="$ctrl.inputId" class="form-label">{{$ctrl.labelName}}</label>
//                              <input class="form-control" type="$ctrl.inputType" id="$ctrl.inputId" name="$ctrl.inputName" ng-model="$ctrl.inputValue" />
//                            </div>`;
// }
// class InputController implements ng.IComponentController {

//     protected labelName: string;
//     protected inputType: string;
//     protected inputId: string;
//     protected inputName: string;
//     protected inputValue: string;

//     static $inject = [];
//     constructor() {
//         // super();
//     }
// }


// export const inputBoxComponent = angular
//     .module('inputBoxComponent', []).component(Safe.Product.InputBoxComponent.ngName, Safe.Product.InputBoxComponent)
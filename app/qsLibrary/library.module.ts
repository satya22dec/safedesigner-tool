import { InputBoxComponent } from "./inputBoxComponent";
import { LabelAndFourInput } from "./labelAndFourInputComponent";
// import "./../framework/coreTypes/__prereq";
// import "./../framework/coreTypes/_exception";

// import "./../framework/coreTypes/ccc_component";
// import "./../framework/coreTypes/ddd_componentSubTypes";
// import "./../framework/library/components/basicInput";

export const qsLibraryModule = angular.module('qsLibraryModule', [])
    .component('inputBoxComponent', InputBoxComponent)
    .component(LabelAndFourInput.ngName, LabelAndFourInput);
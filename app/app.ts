import 'angular-smart-table';
import 'angular-paginate-anything';
import 'angular-pageslide-directive';

import { safeDesignerModule } from "./safeDesigner/safeDesigner.module";
import { qsLibraryModule } from "./qsLibrary/library.module";
import { serviceModule } from "./safeDesigner/services/service.module";

const modules = [
    safeDesignerModule,
    qsLibraryModule,
    serviceModule
];

const legacyDependencies = [
    'ngRoute', 'ngMaterial', 'ui.bootstrap', 'smart-table',
    'bgf.paginateAnything','pageslide-directive'
];

const appComponentOptions: ng.IComponentOptions = {
    templateUrl: './app/app.html'
};
angular
    .module('app', modules.map(module => module.name).concat(legacyDependencies))
    .component('app', appComponentOptions);




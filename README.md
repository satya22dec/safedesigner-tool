# Safe Designer Tool

## Setup 
You only need to have node.js and typescript installed. 

Instal node.js : https://nodejs.org/en/download/

npm install -g typescript

Download/clone the project : https://satya22dec@bitbucket.org/satya22dec/safedesigner-tool.git

cd safedesigner-tool

Install node modules:
```sh
$ npm install
```

Start the server:
```sh
$ npm start
```

Open Browser at http://127.0.0.1:3000 / http://localhost:3000/

## Deployment
```sh
$ npm run deploy
```
